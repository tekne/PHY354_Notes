\documentclass{article}
\usepackage[utf8]{inputenc}

\title{PHY354 Notes: Rigid Body Motion}
\author{Jad Elkhaleq Ghalayini}
\date{March 12 2018}

\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage{cancel}

\newcommand{\lgr}[0]{\mathcal{L}}
\newcommand{\bigo}[0]{\mathcal{O}}
\newcommand{\prt}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\vf}[1]{\mathbf{#1}}
\newcommand{\total}[1]{\frac{d}{dt}\left(#1\right)}

\newtheorem{theorem}{Theorem}
\newtheorem*{claim}{Claim}
\newtheorem*{note}{Note}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}

\begin{document}

\maketitle

Last time:
\[T = \frac{1}{2}\mu\vf{V}_{cm}^2 + \frac{1}{2}\Omega_i\Omega_jI_{ij}\]
\[I_{ij} = \sum_am_a(\delta_{ij}\vf{r}_a^2 - r_{a_i}r_{a_j})\]
If \((x', y', z')\) are aligned with the prinicpal axes, we have
\[I = diag(I_1, I_2, I_3)\]
How are \(I_i\) related to the symmetries of the rigid body
\begin{itemize}
    \item If we have a spherically symmetric object, \(I_i\) are all the same. This is called a ``spherical top''
    \item We could have \underline{two} \(I_i\) be equal, like in a diatomic molecule, or a cone. This is called a ``symmetric top''
    \item You could have all \(I_i\) be different, like for some amorphous blob, and we call this an ``asymmetric top''
\end{itemize}
Homework: Triangle Inequalities:
\[I_i + I_j \geq I_k \forall \text{ distinct } i, j, k\]
All of this is about choosing different orientations of the axes, but what about choosing a different origin for the axes. This should be familiar from first year:
\section{The Parallel Axis Theorem}
Say I know \(I\) in the CM system.
How does \(I\) change if I change the \underline{origin} (not orientation) of the \((x', y', z')\) system:
\[\Tilde{\vf{r}} = \vf{r} - \vf{b}\]
\[\implies \Tilde{I}_{ij} = \sum_am_a(\delta_{ij}\Tilde{\vf{r}}_a^2 - \Tilde{r}_{a_i}\Tilde{r}_{a_j})\]
\[= \sum_a[\delta_{ij}(\vf{r}_a^2 - \cancel{2\vf{r}_a\vf{b}} + \vf{b}^2) - r_{a_i}r_{a_j} + \cancel{r_{a_i}b_j + r_{a_j}{b_i}} - b_ib_j]\]
\[\implies \Tilde{I}_{ij} = I_{ij} + \mu(\delta_{ij}b^2 - b_ib_j)\]
\section{Angular Momentum of a Rigid Body}
We know how to compute kinetic energy, so let's study \underline{free} rigid bodies (\(U = 0\)) some more. Their ROTATIONAL angular momentum is conserved. How to compute?
\subsection{Easy: Hamiltonian Formalism}
\[U = 0 \implies \lgr = T = \frac{1}{2}\mu\vf{V}_{cm}^2 + \frac{1}{2}I_{ij}\Omega_i\Omega_j\]
\(\Omega_i\) is a vector of angular velocities \(\implies\) ``\(\dot{q}\)'' type of things. We don't yet know what the``\(q\)'s'' are (Euler Angles), and we'll get to that later. All we need to know is that if \(\Omega_i\) are ``\(\dot{q}\)'' times things, then there exists a corresponding conjugate momentum
\[p = \prt{\lgr}{\dot{q}} \iff M_i = \prt{\lgr}{\Omega_i}\]
This is quite easy to work out:
\[\prt{\lgr}{\Omega_i} = \prt{T}{\Omega_i} = \prt{}{\Omega_i}\left(\cancel{\frac{1}{2}\mu\vf{V}_{cm}} + \frac{1}{2}I_{ij}\Omega_{i}\Omega_{j}\right) = \frac{1}{2}I_{kl}(\Omega_{l}\delta_{ki} + \Omega_{k}\delta_{li})\]
\[\implies M_i = I_{il}\Omega_l\]
In other words,
\[\vf{M} = \vf{I}\vf{\Omega}\]
``Angular momentum of the rigid body with respect to the center of mass.'' Now we can construct \(H\) for a \underline{free} rigid body (or for the part of \(H\) corresponding to \(T\) if \(U\) is independent of \(\Omega_i\)):
\[H(p, q) = p\dot{q} - L(q, \dot{q})\]
\[\dot{q} \to \Omega_i = (I^{-1})_{il}M_l\]
(If \(I_z = 0\), only \(xy\) shows up)
\[\text{if } \lgr = \frac{1}{2}I_{ij}\Omega_i\Omega_j \implies H = \frac{1}{2}M_i(I^{-1})_{ij}M_j\]
\subsection{Harder: Explicit Calculation}
\begin{itemize}
    \item Is this \(M_i\) really what we think of as angular momentum?
    \item What if we want to know the angular momentum with respect to a different origin?
\end{itemize}
What is the \underline{total} angular momentum in the lab frame with respect to the origin \(O\)?
\[\vf{M}_O = \sum_am_a\vf{\rho}_a \times \vf{v}_a = \sum_am_a\vf{\rho}_a \times \vf{v}_{cm} + \sum m_a\vf{\rho}_a \times (\vf{\Omega} \times \vf{r}_a)\]
\[= \mu\vf{R}_{cm} \times \vf{V}_{cm} + \cancel{\sum_{a}\vf{R}_{cm} \times (\vf{\Omega} \times \vf{r}_a)} + \sum_am_a\vf{r}_a \times (\vf{\Omega} \times \vf{r}_a)\]
\[\implies \vf{M}_O = \mu\vf{R}_{cm} \times \vf{V}_{cm} + \sum_{a}m_a\vf{r}_a \times (\vf{\Omega} \times \vf{r}_a)\]
\[= \text{ ``Orbital AM'' }\vf{M}_{\text{orbital}} + \text{ ``Total AM w.r.t. CM'' }\vf{M}_{cm}\]
\[\vf{M}_{cm} = \vf{M} = \sum_am_a\vf{r}_a + (\vf{\Omega} \times \vf{r}_a)\]
\[M_i = \sum_am_a\epsilon_{ijk}r_{aj}(\Omega \times \vf{r}_a)_k = \sum_am_a(\delta_{il}\delta_{jm} - \delta_{im}\delta_{jl})r_{aj}\Omega_{\ell}r_{am}\]
\[= \sum_am_a[\Omega_i\vf{r}_a^2 - \Omega_jr_{aj}r_{ai}] = \sum_am_a[r_a^2\delta_{ij} - r_{ai}r_{aj}]\Omega_j \implies M_i = I_{ij}\Omega_i\]
\end{document}