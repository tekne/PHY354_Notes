\documentclass{article}
\usepackage[utf8]{inputenc}

\title{PHY354 Notes: Symmetries and Conservation Laws}
\author{Jad Elkhaleq Ghalayini}
\date{January 24 2018}

\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage{cancel}

\newcommand{\lgr}[0]{\mathcal{L}}
\newcommand{\prt}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\vf}[1]{\mathbf{#1}}

\newtheorem{theorem}{Theorem}
\newtheorem*{claim}{Claim}
\newtheorem*{note}{Note}
\newtheorem*{definition}{Definition}

\begin{document}

\maketitle

Last time, we discussed how the homogeneity of time and space lead to the conservation of energy and momentum.

Clarifications:
\begin{enumerate}
    \item Polar co-ordinates in a plane
    \[P_\phi - mv^2\dot{\phi}\]
    \item Closed vs open systems
    \begin{itemize}
        \item \underline{closed system:} everything that matters is a dynamical prticle/component \(\implies\) full set of symmetries
        \item \underline{open system:} there are ``external components'' that are not dynamical, degrees in freedom in \(\lgr\).
    \end{itemize}
\end{enumerate}
\begin{itemize}
    \item [Next:] What do we learn from isotropy of space
    \item [First:] When describing closed systems, there is an especially convenient co-ordinate system, the \underline{center of mass system}.
    \begin{definition}{Center of Mass System: }
    The reference frame where \(\vf{P} = 0\).
    \end{definition}
    Consider a system in an arbitrary ``original'' co-ordinate system \(\vf{r}', \vf{v}'\) and define a ``new'' co-ordinate system \(\vf{r}, \vf{v}\) where
    \[\vf{r} = \vf{r}', \vf{v} = \vf{v}' + \vf{V}, \vf{P} = \sum_im_i\vf{v}_i = 0\]
    \[\vf{P}' = \sum_im_i(\vf{v}_i - \vf{V}) = \vf{P} - \mu\vf{V}, \mu = \sum_im_i\]
    Solve for \(\vf{P}' = 0\)
    \[\implies \vf{V} = \frac{\vf{P}}{\mu} \implies \vf{V}_{cm} = \frac{\sum_im_i\vf{v}_i}{\sum_im_i} = \frac{d}{dt}\vf{R}_{cm}]\]
    \[\vf{R}_{cm} = \frac{\sum_im_i\vf{r_i}}{\sum_im_i}\]
    e.g. for \(N = 2\),
    \[\vf{R}_{cm} = \frac{m_1}{m_1 + m_2}\vf{r}_1 + \frac{m_2}{m_1 + m_2}\vf{r}_2\]
    \begin{itemize}
        \item [Sun-earth system: ] \(\vf{R}_{cm} \approx \vf{r}_{Sun}\)
        \item [Pluto, Charon] \(\vf{R}_{cm}\) is clearly in between the two bodies, they orbit around a central point
    \end{itemize}
    For a closed system, by definition, \(\vf{R}_{cm}\) moves in a straight line
    \begin{itemize}
        \item Typically work in a co-ordinate system where \(\vf{R}_{cm} = 0\)
    \end{itemize}
    \underline{Claim: } The \underline{energy} of a closed system can then be written as
    \[E = \frac{1}{2}\mu\vf{V}_{cm}^2 + E_{internal}\]
    \begin{proof}
    \[E = \sum\frac{1}{2}m_i\vf{v_i}^2 + U\]
    \[\vf{v_i} = \vf{v_i}' + \vf{V}_{cm}\]
    \[E = \sum_i\frac{1}{2}m_i(\vf{v_i}' + \vf{v}_{cm})^2 + U = \sum_i\frac{1}{2}m_i\left[\vf{v}_i^2 + \vf{V}_{cm}^2 + 2\vf{v}_i'\cdot\vf{V}_{cm}\right] + U\]
    \[= \frac{1}{2}\sum_im_i\vf{V}_{cm}^2 + \left[\sum_i\frac{1}{2}m_i\vf{v_i}^2 + U = E_{int}\right]\]
    \end{proof}
\end{itemize}
Now we can begin with
\section{Isotropy of Space}
\(\lgr\) has no preffered direction
\begin{itemize}
    \item [\(\implies\)] \(\lgr\) does not change if system as a whole is \underline{rotated}
\end{itemize}
What is rotation:
\begin{itemize}
    \item Need an origin
    \item Need an angle
    \item Need an origin unit vector \(\hat{n}\)
\end{itemize}
Example:
\[\hat{n} = \hat{z}, \vf{r}' = \vf{r} \text{ rotated by } \phi\]
Let \(\vf{r} = (x, 0, 0)\). Then
\[\vf{r}' = (x\cos\phi, y\sin\phi, 0) \implies |\vf{r}'| = |\vf{r}|\]
Consider a small rotation \(\delta\phi << 1\). Then
\[\vf{r}' = (x + \mathcal{O}(\delta\phi^2), x\delta\phi + \mathcal{O}{\delta\phi^2}, 0) = \vf{r} + \delta\vf{r}\]
\[|\vf{r}| = \sqrt{x^2 + y^2 + z^2} + \mathcal{O}(\delta\phi^2)\]
Hence, for asmall rotation around the \(z\) axis,
\[\vf{r} = (0, x\delta\phi, 0)\]
We can write this as follows:
\begin{itemize}
    \item [Define] \(\vf{\delta\phi} = \delta\phi\hat{n}\). Then
    \[\vf{r}' = \vf{r} + \vf{\delta r}, \vf{\delta r} = \vf{\delta\phi} \times \vf{r}\]
\end{itemize}
So for an arbitrary rotation around small angle \(\delta\phi\), we have
\[\vf{r}' = \vf{r} + \vf{\delta r}\]
\[\vf{\delta r} = \vf{\delta\phi} \times \vf{r}\]
\[|\vf{\delta r}| = |\vf{r}| \times |\vf{\delta\phi}| \times \sin\frac{\pi}{2}\]
Now we can do arbitrary small rotations!
\[\vf{r} \to \vf{r'} = \vf{r} + \vf{\delta r}, \ \ \vf{\delta r} = \vf{\delta \phi} \times \vf{r}\]
\[\vf{v} \to \vf{v'} = \vf{v} + \vf{\delta v}, \ \ \vf{\delta v} = \vf{\delta \phi} \times \vf{v}\]
\[\delta\lgr = \lgr(\vf{r}',\lgr{v}') - \lgr(\vf{r},\vf{v}) = \sum_i\left[\prt{\lgr}{\vf{r}_i}\cdot\vf{\delta\vf r}_i + \prt{\lgr}{\dot{\vf{r}}_i}\cdot\delta\dot{\vf{r}_i}\right]\]
\[= \sum_i\left[\prt{\lgr}{\vf{r}_i}\cdot(\delta\vf{\phi} \times \vf{r}_i) + \prt{\lgr}{\dot{\vf{r}_i)}}\cdot(\delta\vf{\phi} \times \dot{\vf{r_i}})\right] = 0\]
Recall
\[(\vf{A} \times \vf{B})\cdot\vf{C} = \vf{A}\cdot(\vf{B}\times\vf{C})\]
\[\implies \delta\lgr = \delta\vf{\phi} \cdot \sum_i\left[\vf{r}_i \times \prt{\lgr}{\vf{r}_i} + \dot{\vf{r}_i} \times \prt{\lgr}{\vf{r}_i}\right] = 0\]
\[= \delta\vf{\phi} \cdot \sum_i\left[\vf{r}_i \times \dot{\vf{\pi}}_i + \dot{\vf{r_i}} \times \vf{r_i}\right]\]
\[= \delta\vf{\phi} \cdot \frac{d}{dt}\sum_i\vf{r}_i \times \vf{p_i} = 0\]
Meaning that the angular momentum of a closed system is conserved, as
\[\frac{d}{dt}\vf{M} = 0, \vf{M} = \sum_i\vf{r}_i\times\vf{p}_i\]
\begin{note}
Angular and transverse momenta are \underline{additive}
\[\vf{M} = \sum_i\vf{r}_i \times \vf{p}_i\]
\[\vf{P} = \sum_i\vf{p}_i = \sum_im_i\vf{v}_i\]
Energy
\[E = \sum_i\dot{q}_ip_i - \lgr\]
is not additive
\end{note}
\underline{Notes on Symmetries: }
\begin{itemize}
    \item \(\vf{M}\) depends on the \underline{origin} of co-ordinates under \(\vf{r} \to \vf{r} + \vf{a}\),
    \[\vf{M} \to \vf{m} + \vf{a} \times \vf{P}\]
    \item In general, \(E, \vf{P}, \vf{M}\) depend on coordinate systems
    \item As for translational case, can have partial system for open system
    \item What about isotropy of time: time having no preferred direction means I can switch \(t\) to \(-t\). But that will not give me a conserved quantity, because only \textit{continuous} symmetries lead to a conserved quantity. There's no way to do a small reflection: \underline{discrete} symmetries don't give rise to conservation, like parity in nuclear physics or charge reversal.
\end{itemize}
\end{document}