\documentclass{article}
\usepackage[utf8]{inputenc}

\title{PHY354 Notes: Equations of Motion for Rigid Bodies}
\author{Jad Elkhaleq Ghalayini}
\date{March 21 2018}

\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage{cancel}

\newcommand{\lgr}[0]{\mathcal{L}}
\newcommand{\bigo}[0]{\mathcal{O}}
\newcommand{\prt}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\vf}[1]{\mathbf{#1}}
\newcommand{\total}[1]{\frac{d}{dt}\left(#1\right)}

\newtheorem{theorem}{Theorem}
\newtheorem*{claim}{Claim}
\newtheorem*{note}{Note}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}

\begin{document}

\maketitle

\section{Last Time}
We discussed Euler angles \(R(\theta, \phi, \psi) = R_z(\psi)R_x(\theta)R_z(\phi)\), giving
\[(\hat{x}'_{\alpha})_i = R_{\alpha i}\]
in the lab frame, giving
\[r_i = (R^{-1})_{ij}r_j + (R_{cm})_i\]

\section{What about angular velocity?}
We want to find the components of \(\vf{\Omega}\) from a change in \(\vf{r}\). Important:
\begin{itemize}
    \item \(\Omega_i'\): components in the body fixed frame
    \item \(\Omega_i\): components in the lab frame
\end{itemize}
\[r_i = (R^{-1})_{ij}r_j'\]
\[v_i = \frac{dr_i}{dt} = (\vf{\Omega} \times \vf{r})_i = \epsilon_{ijk}\Omega_jr_k\]
Also,
\[v_i = \prt{d(R^{-1})_{ij}}{dt}r_j' =  \prt{d(R^{-1})_{ij}}{dt}R_{jk}r_k\]
So we can see that
\[\prt{d(R^{-1})_{ij}}{dt}R_{jk} = \epsilon_{ijk}\Omega_j\]
To find \(\vf{\Omega}\), use
\[\frac{dR^{-1}}{dt} = \frac{dR^{-1}}{d\theta}\dot{\theta} + \frac{dR^{-1}}{d\phi}\dot{\phi} + \frac{dR^{-1}}{d\psi}\dot{\psi}\]
Since \(R^{-1}_{x,z} =R_{x,z}(-\phi)\), we have
\[R^{-1}(\theta, \phi, \psi) = R_z(-\phi)R_x(-\theta)R_z(-\psi)\]
Result:
\[\vf{\Omega} = \dot{\vf{\theta}} + \dot{\vf{\phi}} + \dot{\vf{\psi}}\]
define
\[\dot{\vf{\phi}} = \dot{\phi}\vf{n}^{\phi}, \dot{\vf{\theta}} = \dot{\theta}\vf{n}^{\theta}, \dot{\vf{\psi}} = \dot{\psi}\vf{n}^{\psi}\]
In the lab frame,
\[\vf{n}^{\phi} = (0, 0, 1)\]
\[\vf{n}^{\theta} = (\cos\phi, \sin\phi, 0)\]
\[\vf{n}^{\psi} = (\sin\phi\sin\theta, -\cos\phi\sin\theta, \cos\theta)\]
note that these are \underline{not necessarily orthogonal.} Hence, in the \underline{lab frame}, we have
\[\Omega_1 = \cos\phi\dot{\theta} + \sin\phi\cos\theta\dot{\psi}\]
\[\Omega_2 = \sin\phi\dot{\theta} - \cos\phi\sin\theta\dot{\psi}\]
\[\Omega_3 = \dot{\phi} + \cos{\theta}\dot{\psi}\]
In the body fixed frame, \(\Omega'_i = R_{ij}\Omega_j\) giving
\[\Omega_1' = \dot{\phi}\sin\theta\sin\psi + \dot{\theta}\cos\psi\]
\[\Omega_2' = \dot{\phi}\sin\theta\cos\psi - \dot{\theta}\sin\psi\]
\[\Omega_3' = \dot{\psi} + \dot{\phi}\cos\theta\]
\section{Free Symmetric Top (Again)}
Now with Euler angles, \(I = I_1 = I_1 \neq I_3\). Let \(\vf{M} = M\hat{x}_3\) and \(\hat{x}_\alpha'\) be principal axes. You already know, but in this language we have
\begin{itemize}
    \item Rotation around \(x_3'\) with angular velocity \(\Omega_3'\)
    \item Precession of \(\hat{x}_3'\) axis around \(\vf{M}\) with angular velocity \(\dot{\phi}\).
\end{itemize}
We work in body-fixed frame:
\[T = \frac{1}{2}(I(\Omega_1^2 + \Omega_2^2) + I_3\Omega_3'^2)\]
\[= \frac{1}{2}I(\dot{\theta}^2 + \dot{\phi}^2\sin^2\theta) + \frac{1}{2}I_3(\dot{\psi} + \dot{\phi}\cos\theta)^2\]
Now, we could write the Euler Lagrange equations from here, but, we always like using symmetry when we can. We have two ways of writing \(\vf{M}\)in the body fixed frame:
\[M_3' = \vf{M} \cdot \hat{x}_3' = M\hat{x}_3\cdot\hat{x}_3' = MR_{33} = M\cos\theta\]
\[M_2' = \vf{M} \cdot \hat{x}_2' = MR_{23} = M\cos\psi\sin\theta\]
\[M_1' = \vf{M} \cdot \hat{x}_1' = MR_{13} = M\sin\psi\cos\theta\]
We also know that \(M_i' = I_i\Omega_i\) (no summation), implying that
\[M_3' = I_3(\dot{\psi} + \dot{\phi}\cos\theta)\]
\[M_2' = I(\dot{\phi}\sin\theta\cos\psi - \dot{\theta}\sin\psi)\]
\[M_1' = I(\dot{\phi}\sin\theta\sin\psi - \dot{\theta}\cos\psi)\]
\[\implies \dot{\psi} + \dot{\phi}\cos\theta = \frac{M}{I_3}\cos\theta\]
\[\times\cos\psi \implies \dot{\phi}\sin\theta\cos^2\psi - \dot{\theta}\sin^2\psi\cos\psi = \frac{M}{I}\sin\theta\cos^2\psi\]
\[\times\sin\psi \implies \dot{\psi}\sin\theta\sin^2\psi + \dot{\theta}\sin\psi\cos\psi = \frac{M}{I}\sin\theta\sin^2\psi\]
Call this collection of equations *. Add *2, *3 to obtain
\[\frac{M}{I} = \dot{\phi}\]
Plug this into *2 to obtain
\[\psi = M\left(\frac{1}{I_3} - \frac{1}{I}\right)\cos\theta\]
Plug \(\dot{\phi}\) with *2, 3 without the extra \(\times\cos\psi, \times\sin\psi\) to obtain
\[0 = \frac{M}{I}(\sin\theta\cos\psi - \sin\theta\cos\psi) = \dot{\theta}\sin\psi\]
\[0 = \frac{M}{I}(\sin\theta\sin\psi - \sin\theta\sin\psi) = -\dot{\theta}\cos\psi\]
Since \(\sin\psi, \cos\psi\) cannot simultaneously be zero, \(\dot{\theta} = 0 \implies \theta\) constant.
\section{The Heavy Symmetric Top}
The free symmetric top is a little boring, because, note, we wrote down the Lagrangian but you never used it. Now, we finally get to write down an Euler-Lagrange equation. Physically, the heavy symmetric top is a top that spins in a uniform gravitational field. The complete problem, with friction and energy dissipation and all that is extremely complicated. We're going to do the simplest version of this, but it's still an incredibly rich system. We'll be working with a frictionless flat table, with a little tiny groove in it, which the top is spinning in. The top can't move, but can spin and process and all that. The demonstration we're going to use is an unbalanced gyroscope, which is pretty much this system.
\end{document}