\documentclass{article}
\usepackage[utf8]{inputenc}

\title{PHY354 Notes: Symmetries and Conservation Laws}
\author{Jad Elkhaleq Ghalayini}
\date{January 22 2018}

\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage{cancel}

\newtheorem{theorem}{Theorem}
\newtheorem*{claim}{Claim}
\newtheorem{definition}{Definition}
\newtheorem*{note}{Note}

\begin{document}

\maketitle

Previously: symmetries restricting the form of \(\mathcal{L}\). Now, symmetries give information on the solutions of our equations of motion. This is Noether's theorem; symetries imply conservation laws.
\begin{enumerate}

    \item \underline{Homogeneity of Time:} \begin{itemize}
        \item [\(\implies\)] \(\mathcal{L}\) for a \underline{closed system} is \(t\)-independent
        \item [\(\implies\)] \(\mathcal{L}(q, \dot{q}, t) = \mathcal{L}(q, \dot{q})\)
        \item [\(\implies\)] \[\frac{d\mathcal{L}}{dt} = \left(\sum_{i}\frac{\partial\mathcal{L}}{\partial\dot{q}_i} + \frac{\partial\mathcal{L}}{\partial\dot{q}_i}\ddot{q}_i\right) + \cancel{\frac{\partial\mathcal{L}}{\partial t}}\]
        Let's call this equation *. Assume\(q(t)\) is a solution to the Euler-Lagrange equations. Plugging into *, we get
        \item [\(\implies\)] \[\frac{d\mathcal{L}}{dt} = \sum_i\left[\left(\frac{d}{dt}\frac{\partial\mathcal{L}}{\partial\dot{q}_i}\right) + \frac{\partial\mathcal{L}}{\partial\dot{q}_i}\ddot{q}_i\right]\]
        \[\frac{d\mathcal{L}}{dt} = \frac{d}{dt}\left[\sum_i\frac{\partial\mathcal{L}}{\partial\dot{q}_i}\dot{q}_i\right] \implies 0 = \frac{d}{dt}\left[\sum_i\frac{\partial\mathcal{L}}{\partial\dot{q}_i}\dot{q}_i - \mathcal{L}\right]\]
        But we have that
        \[E = \sum_i\frac{\partial\mathcal{L}}{\partial\dot{q}_i}\dot{q}_i - \mathcal{L} \implies \frac{dE}{dt} = 0\]
        i.e. energy is conserved!
        Is this the usual energy that we know? Yes: let \(\mathcal{L} = \sum_i\frac{m_i}{2}\dot{q}_i^2 - U(\{q_i\})\)
        \[E = \sum_i\left[m_iq_i^2 - \frac{m_i}{2}\dot{q_i}^2 + U\right] = \sum_i\left[\frac{1}{2}m_i\dot{q}_i^2\right] + U\]
        So we proved that energy is conserved using only time symmetry.
    \end{itemize}
    
    \item \underline{Homogeneity of Space:}
    \(\mathcal{L}\) for a closed system is invariant under \underline{arbitrary} shifts \(\mathbf{r}_i \to \mathbf{r}_i' = \mathbf{r}_i + \mathbf{a}\). What is the associated conserved property?
    
    Let \[\mathcal{L} = \mathcal{L}(\{\mathbf{r}_i,\dot{\mathbf{r}_i}\}), \mathcal{L}' = \mathcal{L}(\{\mathbf{r}_i+\mathbf{a}_i,\mathbf{r}_i\})\]
    This is a continuous symmetry, so we can work with infinitesimal transformations, unlike, say, reflection. Noether's theorem only works with continuous symmetries.
    \[\mathbf{a} = \epsilon\mathbf{b}, \epsilon \to 0\]
    \[\mathcal{L}' = \mathcal{L}(\{r_i + \epsilon b_i, \dot{r_i}\}) = \mathcal{L}(\{r_i, \dot{r_i}\}) + \cancel{\epsilon\sum_i\frac{\partial\mathcal{L}}{\partial r_i}\mathbf{b}} + \mathcal{O}(\epsilon^2)\]
    due to the homogeneity of space, implying
    \[\sum_i\frac{\partial L}{\partial r_i} = 0\]
    Again assume \(r_i\) are solutions to the Euler Lagrange equations. Then we have that
    \[\sum_i\frac{\partial\mathcal{L}}{\partial\dot{q}} = \text{ const w/ respect to time}\]
    \begin{definition}
    \[\mathbf{p}_i = \frac{\partial\mathcal{L}}{\partial\dot{\mathbf{r}_i}}\]
    \end{definition}
    So \(\mathbf{p} = \sum_i\mathbf{p}_i = \text{ const }\), i.e. momentum is conserved!
    \begin{note}
    This is NOT TRUE if \(U\) depends on position, NOT ONLY on \(\mathbf{r}_i - \mathbf{r}_j\), i.e. if we have an ``external potential'' (freed, non closed system).
    \end{note}
\end{enumerate}
Generally, if \(q_i\) are ``generalized co-ordinates'', we call
\[\frac{\partial\mathcal{L}}{\partial\dot{q_i}} = p_i\]
''generalized momentum''.
In general, \(\frac{\partial\mathcal{L}}{\partial\dot{q}_i}\) depends on both \(q_i\dot{S}, \dot{q}_i\dot{S}\) so
\[p_i = p_i(q_i, \dot{q_i})\]
Example:
\[L = \frac{m}{2}\dot{\mathbf{r}}^2  - U(\mathbf{r})\]
In Cartesian co-ordinates
\[\frac{\partial\mathcal{L}}{\partial\dot{\mathbf{r}}} = m\dot{\mathbf{r}} = \mathbf{p}\]
Consider polar co-ordinates on the \(xy\) plane
\[\mathcal{L} = \frac{m}{2}(\dot{r}^2 + r^2\dot{\phi}^2) - U(r)\]
\[\implies \frac{\partial\mathcal{L}}{\partial\dot{r}} = p_r = m\dot{r}\]
``radial momentum'' depends only on \(\dot{r}\).
\[\frac{\partial\mathcal{L}}{\partial\dot{\phi}} = p_\phi = mr^2\dot{\phi}\]

Comments:
\begin{enumerate}

    \item In a non-closed system, \(U\) can explicitly depend on some co-ordinates, implying only some (or no) translational symmetries survive, implying only some parts of \(\mathbf{p}\) are conserved
    
    \item If \(p_i = \frac{\partial\mathcal{L}}{\partial q_i}\) then
    \[\text{EL } \implies \frac{d}{dt}p_i = f_i = \frac{\partial\mathcal{L}}{\partial q_i} \text{ - the generalized forces}\]
    
    \item Momentum conservation is just Newton's third law.
    \[\sum_if_i = 0 \implies \frac{d}{dt}\sum_ip_i = 0\]
    
\end{enumerate}

Conservation can be useful in all kinds of problems, including in quite abstract ways
\section*{Example of Application: Refraction}
Consider a particle moving in a potential
\[U = \left\{\begin{array}{c}U_1 \text{ if } z < 0 \\ U_2 \text{ if } z > 0\end{array}\right.\]
It's completely constant except that there's a jump. By looking at this, what are the symmetries:
\begin{itemize}
    \item We have \(t, x, y\) invariance
    \item We do \underline{not} have \(z\) invariance
\end{itemize}
Consider a particle moving towards the line \(z = 0\) with some angle \(\theta_1\) to the normal with some velocity \(v_1\), with motion in the \(xz\) plane, and have that it exits with velocity \(v_2\) and angle \(\theta_2\).
\begin{itemize}
    \item \(x, y\) momentum is conserved, so
    \[v_1\sin\theta_1 = v_2\sin\theta_2\]
    \item By energy conservation, 
    \[\frac{m}{2}v_1^2 + U_1 = \frac{m}{2}v_2^2 + U_2\]
    \[ \implies \frac{\sin\theta_1}{\sin\theta_2} = \frac{v_2}{v_1} = \sqrt{1 + \frac{2}{mv_1^2}(v_1 - v_2)} = \sqrt{1 - \frac{\Delta PE}{KE_{initial}}}\]
\end{itemize}

\end{document}