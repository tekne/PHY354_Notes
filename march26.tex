\documentclass{article}
\usepackage[utf8]{inputenc}

\title{PHY354 Notes: Equations of Motion for Rigid Bodies}
\author{Jad Elkhaleq Ghalayini}
\date{March 26 2018}

\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage{cancel}

\newcommand{\lgr}[0]{\mathcal{L}}
\newcommand{\bigo}[0]{\mathcal{O}}
\newcommand{\prt}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\vf}[1]{\mathbf{#1}}
\newcommand{\total}[1]{\frac{d}{dt}\left(#1\right)}

\newtheorem{theorem}{Theorem}
\newtheorem*{claim}{Claim}
\newtheorem*{note}{Note}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}

\begin{document}

\maketitle

Last time we started the \underline{heavy symmetric top}.
\begin{itemize}
    \item Top is free to rotate around a point \(P\)
    \item Top is fixed to position \(P\)
    \item Use \(P\) as origin of \(x'\) system, NOT the center of mass (is \(\vf{\ell}'\) away from CM)
    \item Drop primes: \(\Omega' \to \Omega\)
    \item Parallel axis theorem, in \(y'\) system with origin \(P\)
    \[I_3 = I_3^{CM}, I = I^{CM} + m\ell^2\]
\end{itemize}
We get the Lagrangian
\[\lgr = T - U = \frac{1}{2}(\Omega_1^2 + \Omega_2^2) + \frac{1}{2}I_3\Omega_3^2 - mg\ell\cos\theta\]
\[= \frac{1}{2}I_1(\dot{\theta}^2 + \sin^2\theta\dot{\phi}^2) + \frac{1}{2}I_{3}(\dot{\psi} + \cos\theta\dot{\phi})^2 - mg\ell\cos\theta\]
What are the conserved quantities?
\begin{itemize}
    
    \item \(\lgr\) does not depend on \(\phi, \psi\) \(\implies\) cyclic coordinates \(\implies\) \(p_{\phi}, p_{\psi}\) are conserved
    \[p_{\psi} = \prt{\lgr}{\dot{\psi}} = I_3(\dot{\psi} + \cos\theta\dot{\phi}) = I_3\Omega_3\]
    \[p_{\phi} = I_1\sin^2\theta\dot{\phi} + I_3\cos\theta(\dot{\psi} + \dot{\phi}\cos\theta) = I_3\Omega_3\cos\theta + I_1\sin^2\theta\dot{\phi}\]
    
    \item \[E = T + U = \frac{1}{2}(\dot{\theta}^2 + \dot{\phi}^2\sin^2\theta) + \frac{1}{2}I_3\Omega_3^2 + mg\ell\cos\theta\]
    
\end{itemize}
This is a very rich but simple system: we have 3 degrees of freedom, and 3 conserved quantities, so the system is \underline{integrable}.

We will follow Tang notation. Define the constants
\[a = \frac{I_3\Omega_3}{I_1} = \frac{p_\psi}{I_1} ,b = \frac{p_{\phi}}{I_1}\]
We solve the \(p_\phi, p_\psi\) relations for
\[\implies \dot{\phi} = \frac{b - a\cos\theta}{\sin^2\theta}\]
\[\implies \dot{\psi} = \frac{I_1}{I_3}a - \frac{(b - a\cos\theta)\cos\theta}{\sin^2\theta}\]
Define ``reduced energy''
\[E' = E - \frac{1}{2}I_3\Omega_3^2\]
\[= \frac{1}{2}I_1\dot{\theta}^2 + V_{eff}(\theta)\]
with
\[V_{eff}(\theta) = \frac{I_1(b - a\cos\theta)^2}{2\sin^2\theta} + mg\ell\cos\theta\]
just like for central potentials! We can solve in quadrature, but to understand the physics, we simplify further, then analyze qualitatively. Define
\[u = \cos\theta \implies \sin\theta\dot{\theta} = \dot{u} \implies \dot{\theta} = \frac{\dot{u}}{\sqrt{1 - u^2}}\]
Plugging this into our equation for \(E'\), weget
\[E' = \frac{1}{2}I_1\left(\frac{\dot{u}^2}{1 - u^2}\right) + \frac{I_1(b - au)^2}{2(1 - u^2)} + mg\ell u\]
\[\implies \dot{u}^2 = \left(\frac{2E'}{I_1} - \frac{2mg\ell u}{I_1}\right)(1 - u^2) - (b - au)^2\]
Define constants
\[\alpha = \frac{2E'}{I_1}, \beta = \frac{2mg\ell}{I_1}\]
giving EOM for HST
\[\dot{u}^2 = f(u) = (1 - u^2)(\alpha - \beta u) - (b - au)^2\]
\[\dot{\phi} = \frac{b - au}{1 - u^2}\]
\[\dot{\psi} = \frac{I_1}{I_3}a - \frac{u(b - au)}{1 - u^2}\]
giving the general solution
\[t = \int_{u_{\min}}^{u_{\max}}\frac{du}{\sqrt{f(t)}}\]
which gives \(u(t)\), which gives \(\phi(t), \psi(t)\).
\subsubsection*{Let's understand the HST qualitatively}
It's all in the \(f(u)\) function
\begin{itemize}

    \item \(f(u)\) is a cubic polynomial \(\alpha + u^3 \implies f \to \pm \infty\) as \(u \to \pm\infty\)
    
    \item Also, \(f(\pm 1) = -(b \mp a)^2 < 0\), with \(u\) physically limited to \((-1, 1)\)
    
    \item Physical range is \(\dot{u}^2 = f(u) > 0\)
    
    \item \(\implies\) the roots of \(f(u)\) between \(\pm 1\) define \(u_{\min}, u_{\max}\)

\end{itemize}
So \(u\) (and hence \(\theta\)) oscialltes between \(u_{\min}, u_{\max}\). Furthermore,
\[\dot{\phi} = \frac{b - au}{1 - u^2}\]
so \(\dot{\phi}\) speeds up or slows down as \(\phi\) (or \(u\)) changes, depending on
\[\frac{b}{a} = \frac{p_{\phi}}{I_3\Omega_3}\]
\(\dot{\phi}\) may or may not switch sign.
\begin{definition}
\begin{itemize}
    \item [\(\dot{\phi} = \)] precession
    \item [\(\dot{\theta} = \)] mutation
\end{itemize}
\end{definition}
We hence have 3 possibilities for motion
\begin{enumerate}
    
    \item No sign change of \(\dot{\phi}\), direction is same throughout motion, top spin speeds up and slows down with \(\theta\)
    
    \item \(\dot{\phi}\) sign changes
    
    \item \(\dot{\phi} = 0\) at one extreme of motion. 
    
    This occurs if you spin the top while holding it at a fixed \(\theta\), then let go. Gravity causes the top to tip over with initial condition \(\dot{\phi} = 0\).
    
\end{enumerate}
We can quantitatively analyze this motion. Since
\[p_{\phi} = I_1\sin^2\theta\dot{\phi} + I_3\Omega_3\cos\theta = I_3\Omega_3\cos\theta_0\]
\[\text{drop} \to \theta\uparrow \to \cos\theta\downarrow \to \dot{\phi}\uparrow \text{ from zero}\]
All of this stuff is the equivalent of eliptical orbits
\end{document}