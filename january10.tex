\documentclass{article}
\usepackage[utf8]{inputenc}

\title{PHY354 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{January 2018}

\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{tikz}

\newtheorem{theorem}{Theorem}

\begin{document}

\maketitle

\section{Clarifications}
\begin{enumerate}
    \item \(\mathcal{O}(\epsilon^2)\) means terms which scale as \(\epsilon^2\) as \underline{\(\epsilon \to 0\)}, e.g.
    \[f(x + \epsilon) = f(x) + f'(x)\epsilon + \frac{f''(x)\epsilon^2}{2} = \mathcal{O}(1) + \mathcal{O}(\epsilon) + \mathcal{O}(\epsilon^2)\]
    
    \item First variational derivative of a functional
    \[\frac{\delta S[x]}{\delta x(t)} = \lim_{\delta x(t) \to 0}\frac{S[x + \delta x] - S[x]}{\delta x(t)}\]
    
\end{enumerate}
\section{The Classical Limit}
Last time, we claimed that minimizing the action gives the particle's true trajectory. So we want to prove that minimizing action is equivalent to solving Newton's EOM \(mx' = -U'(x)\). We begin with the Main Theorem of Variational Calculus
\begin{theorem}(Main Theorem of Variational Calculus):
\[\int_{t_1}^{t_2}dtf(t)\delta q(t) = 0 \forall \delta q(t) \implies f(t) = 0\]
\end{theorem}
\begin{proof}
We can choose any \(\delta q\), so let \(\delta q\) be a ``delta-like'' function, a nice differentiable, narrow hump. Whatever \(t*\) we use,
\[\int f(x)\delta(t - t*)dt = f(t*) = 0 \forall t*\]
\end{proof}
Now, we attempt to prove that least action is equivalent to Newton's EOM.
\begin{proof}
Let 
\[S[q] = \int_{t_1}^{t_2}dt\mathcal{L}(q, \dot{q}, t)\]
(the most general form of \(T - U\)). Consider a trajectory \(q(t)\) \& arbitrary \(\delta q(t\) such that \(\delta q(t_{1,2}) = 0\).
\[\delta S - S[q + \delta q] - S[q] = \int_{t_1}^{t_2}dt(\mathcal{L}(q + \delta q, \dot{q} + \dot{(\delta q)}, t) - \mathcal{L}(q, \dot{q}, t))\]
\[L(q + \delta q, \dot{q} + \dot{(\delta q)}, t) = 
L(q, \dot{q}, t) + 
\frac{\partial\mathcal{L}(q, \dot{q}, t)}{\partial q}\delta q
+ \frac{\partial\mathcal{L}(q, \dot{q}, t)}{\partial\dot{q}}\frac{d}{dt}\delta q
+ \mathcal{O}(\delta q^2)\]
\[\implies \delta S = \int_{t_1}^{t_2}dt\left\{\frac{\partial\mathcal{L}(q, \dot{q}, t)}{\partial q}\delta q
+ \frac{\partial\mathcal{L}(q, \dot{q}, t)}{\partial\dot{q}}\dot{\delta q}\right\}\]
We can't use the variational theorem yet, because we have arbitrary \(\delta q\) and \(\dot{\delta q}\). But they're not independent, so we can use integration by parts. The second term is just
\[\int_{t_1}^{t_2}dt\frac{d\mathcal{L}}{d\dot{q}}\frac{d}{dt}\delta q = \left.\frac{\partial\mathcal{L}}{\partial \dot{q}}\right|_{t = t_1}^{t = t_2} - \int_{t_1}^{t_2}\frac{d}{dt}\frac{\partial \mathcal{L}}{\partial\dot{q}}dq\]
So we have
\[\delta S = \int_{t_1}^{t_2}dt\left[\frac{\partial\mathcal{L}}{\partial q} - \frac{d}{dt}\frac{\partial\mathcal{L}}{\partial \dot{q}}\right]dq\]
Which implies, if \(\delta S = 0\), the \underline{Euler-Lagrange Equations}
\[\frac{\partial\mathcal{L}}{\partial q} = \frac{d}{dt}\frac{\partial\mathcal{L}}{\partial\dot{q}}\]
Take \(\mathcal{L} = T - U = \frac{m\dot{q}^2}{2} - U(q)\)
\[EL \to \frac{\partial\mathcal{L}}{\partial q} = -\frac{\partial U}{\partial q} = -U' = \frac{\partial \mathcal{L}}{\partial q} = m\dot{q} \implies m\ddot{q} = -U'\]
\end{proof}
\subsection{Quantum Aside}
\underline{Huh?} - My reaction many years ago. How does the particle know to minimize the action? We cannot understand why within classical mechanics, but this is a 19th century clue to QM. Let me briefly explain why:

We today understand that classical physics or classical mechanics is only a limit of QM. Formally speaking, this is obtained by taking the Planck's constant, \(\hbar = h/2\pi\), to zero. \(\hbar\) is the minimum quantum action, \(\hbar = 10^{-34}J \cdot s\). On this own this doesn't make sense, what we mean is
\[\frac{S}{\hbar} \to \infty\]
Example: The Hydrogen Atom: We know that the speed of the electron in bound state is roughly
\[v = \alpha c, \alpha = \frac{e^2}{4\pi\epsilon_0\hbar c} \approx \frac{1}{132}\]
Bohr radius
\[a_0 = \frac{\hbar}{mc\alpha} = \frac{1}{2} \cdot 10^{-9} m, m_e = 10^{-30}kg, m_ec^2 = 511keV\]
\[S \approx KE \cdot \ \text{time} \ \approx \int_{0}^T\frac{mv^2}{2}dt \approx T\frac{m_ev^2}{2} \approx \frac{m_ev^2}{2}\frac{2\pi a_0}{v} \approx \pi m_eva_0\]
QM tells us angular momentum \(\mathbf{r} \times \mathbf{p}\) is quantized, so \(m_eva_0 \approx \hbar\)
So
\[\frac{S}{\hbar} = \mathcal{O}(1) \implies \ \text{Quantum}\]
Earth \& Sun:
\[\frac{S}{\hbar} \approx 10^{75} \approx \infty \implies \ \text{Classical}\]
Now what does this have to do with the Least Action Principle?

In QM, the particle ``takes all paths'': you're familiar with this from the double slit experiment: the electron diffracts. The reason it is probable the electron will land at point \(2\) and not at \(3\) (see figure 1) is that the wave function \(\phi\) of the electron undergoes constructive interference, so \(|\phi|^2 \approx \ \text{prob}\) is large. Feynman formulated all QM in terms of the Feynman path integral
\[A = |\phi|^2\alpha\sum_{\text{all paths from} 1 \to 2}e^{i/\hbar}S_{\text{path}}\]
Take classical limit: ``\(\hbar = 0\)''.

\begin{figure}
    \centering
    \begin{tikzpicture}
        \draw(0, 0) -- (0, 1.9);
        \draw(0, 2) -- (0, 3);
        \draw(0, 3.1) -- (0, 5);
        \filldraw(-2, 2.5) circle(2pt) node[anchor = east]{1};
        \draw(-2, 2.5) -- (0, 3.05) -- (2, 2.5);
        \draw(-2, 2.5) -- (0, 1.95) -- (2, 2.5);
        \draw(2, 0) -- (2, 5);
        \filldraw(2, 2.5) circle(2pt) node[anchor = west]{2};
        \filldraw(2, 4) circle(2pt) node[anchor = west]{3};
    \end{tikzpicture}
    \caption{The double slit experiment}
    \label{fig:doubleslit}
\end{figure}

So take a random path \(x(t)\), then all it's ``neighboring paths'' (infinitesimal statement) interfere destructively, implying \(A \to 0\). But if we take the path which extremizes \(S\), then 
\[\frac{\delta S}{\delta \alpha} = 0\]
so all the neighboring paths have ``the same action'', implying constructive interference.

In the classical limit, the QM path integral is DOMINATED by the path which has the least action.
\subsection{More Practical Reasons the Least Action Principle is so useful}

In mechanics it is much more useful to formulate problems in a co-ordinate independent manner, and also on \underline{curved spaces} or \underline{constrained systems}.
\begin{itemize}
    \item It's essential for QM
    \item It's essential for GR
    \item It's essential for classical electrodynamics
    \item It's the main tool for elementary particle physics/Quantum field theory/String theory
\end{itemize}
So in a sense, this is your first genuine theoretical physics course. Welcome!
\end{document}