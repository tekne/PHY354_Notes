\documentclass{article}
\usepackage[utf8]{inputenc}

\title{PHY354 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{January 15 2018}

\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{tikz}

\newtheorem{theorem}{Theorem}
\newtheorem*{claim}{Claim}

\begin{document}

\maketitle

\section{Least Action}
We have shown that minimizing the action yields the Euler-Lagrange equations
\[\frac{\partial\mathcal{L}}{\partial q} = \frac{d}{dt}\frac{\partial\mathcal{L}}{\partial \dot{q}}\]
This is the same for \(N\) particles having co-ordinates \(q_i, i \in \mathbb{N}, i \leq 3N\), giving 3N equations
\[\frac{\partial\mathcal{L}(q, \dot{q}, t)}{\partial q} = \frac{d}{dt}\frac{\partial\mathcal{L}(q, \dot{q}, t)}{\partial \dot{q}}\]
What is the most general form of \(\mathcal{L}\) for an arbitrary number of particles. We're going to be using the basic assumptions of classical mechanics, pre-Einstein, pre-QM, but this will show you how theoretical physics was built up. The basic principles, however, remain unchanged: find symmetries, giving tight constraints on how particles move. So,

Strategy
\begin{enumerate}
    \item Aside about CM assumptions
    \item Most general \(\mathcal{L}\)  for \(N\) \underline{free} particles
    \item Most general interaction
    \item Driven systems and constraints
\end{enumerate}

Assumptions of CM:
\begin{enumerate}
    \item Space is \(\mathbb{R}^3 \to \mathbf{r} = (x, y, z)\)
    \item Time is determined by a ``standard clock'': both absolute, both homogeneous \(\implies \mathbb{R}^3 + \mathbb{R}\) 
    \item An inertial frame of reference is one moving in uniform motion with respect to absolute space. \(v \in (0, \infty)\) - no upper limit. Note: the surface of the earth is roughly inertial if gravity is not important (e.g. billiards on the surface of a table).
    \item Galilean Principle of Relativity: physics (EOM \(\iff\) Newton's laws) are the same in all inertial frames
    \item Further developments: 
    \begin{itemize}
        \item E\&M, Lorentz, Einstein: limit of speed as \(c\) (vectors add as in a hyperbolic sphere of radius \(c\)), and spacetime has Minkowski geometry \(\mathbb{R}^{3, 1}\). \item In GR, spacetime and matter are linked, and the simple way to sum that up is that matter curves spacetime, and spacetime tells matter how to moves. 
        \item If we take it further, we get quantum gravity (string theory) in which space time is emergent.
    \end{itemize}
\end{enumerate}

Most general \(\mathcal{L}\) for \(N\) particles, using these rules:
\begin{itemize}
    \item All distances between particles so large that \(U, F ~ u' \to 0\)
    \item Abstract, but lesson in physics: SYMMETRIES \(\implies\) DYNAMICS
\end{itemize}
Homogeneity and inertial frame tells us that this complicated function 
\[\mathcal{L}(\mathbf{r}, \dot{\mathbf{r}}, t) = \mathcal{L}{\dot{\mathbf{\dot{r}}}}\]
By isotropy,
\[\mathcal{L}(\mathbf{\dot{r}}) = \mathcal{L}(\mathbf{\dot{r}}^2)\]
Galilean relativity:
\[\text{EOM of} \ \mathcal{L}(\mathbf{v}^2) \ \text{and} \ \mathcal{L}(\mathbf{v}{'}^{2}) = \mathcal{L}([\mathbf{v - v_0}]^2)\]
have to be the same. Consequences?
\begin{claim}
This is only true if \(\mathcal{L} = \text{const} \cdot \mathbf{v}^2\) (single free particle)
\end{claim}
\begin{proof}
\begin{enumerate}
    \item Consider \[\mathcal{L}(q, \dot{q}, t), \mathcal{L}'(q, \dot{q}, t) = \mathcal{L} + \frac{d}{dt}f(q, t)\]
    Then \(\mathcal{L}, \mathcal{L}'\) have the same equation of motion
    \begin{proof}
    \[S' = S[\mathcal{L}] + \left.f\right|_{t_1}^{t_2} = S[\mathcal{L}] + const \implies \delta S' = \delta S\]
    \end{proof}
    \item Consider a slow frame
    \[\mathbf{v_0} = \epsilon\mathbf{v_1}, \epsilon \to 0\]
    We perform a Taylor expansion
    \[\mathcal{L}([\mathbf{v - v_0}]^2) = \mathcal{L}([\mathbf{v} - \epsilon\mathbf{v_1}]^2) = \mathcal{L}(\mathbf{v}^2 - 2\epsilon\mathbf{vv_1} + \epsilon^2\mathbf{v_1})\]
    \[= \mathcal{L}(\mathbf{v}^2) - 2\epsilon\mathbf{v \cdot v_1}\frac{\partial L}{\partial \mathbf{v_1}^2} + \mathcal{O}(\epsilon^2)\]
    \[\mathcal{L}' = \mathcal{L}(\mathbf{r}^2) - \frac{d}{dt}(2\epsilon\mathbf{\Gamma\cdot\eta})\frac{\partial\mathcal{L}}{\partial r^2} + \mathcal{O}(\epsilon^2)\]
    If \(\mathcal{L}\) is proportional to \(\mathbf{v}^2\), \(\mathcal{L}'\) has the same EOM as \(\mathcal{L}\) by Part 1, as
    \[\frac{\partial\mathcal{L}}{\partial r^2} = const \implies \frac{d}{dt}(2\epsilon\mathbf{\Gamma\cdot\eta})\frac{\partial\mathcal{L}}{\partial r^2} = \frac{d}{dt}(2\epsilon\mathbf{\Gamma\cdot\eta}\frac{\partial\mathcal{L}}{\partial r^2})\]
    \item Does \(L = const \cdot \mathbf{v}^2\)?
    Yes: Let \(v_0\) be large.
    \[L[(\mathbf{v - v_0})^2] = const \cdot (\mathbf{v - v_0})^2 \alpha \mathbf{v}^2 - 2\mathbf{vv_0} + \mathbf{v_0}^2\]
\end{enumerate}
So, homogeneity and isotropy of space and time imply
\[L = constv^2 = \frac{1}{2}mv^2\] for a free particle.
We derived this purely from symmetry: we're going to be doing this a lot...
\end{proof}
\end{document}