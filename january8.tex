\documentclass{article}
\usepackage[utf8]{inputenc}

\title{PHY354 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{January 8 2018}

\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}

\begin{document}

\maketitle

\section{Introduction}
What's this about? Particles - any object where the internal structure doesn't matter and doesn't change. It could be an electron, a ball, a chair, a cow, the earth or the sun. Since internal structure doesn't change, we care about only one value: the particle's position \(\mathbf{r} = (x, y, z)\) which depends on time.
\[\mathbf{r}(t) \textit{gives a trajectory}\]
We have distance
\[|\mathbf{r}_1 - \mathbf{r}_2| = \sqrt{(x_2 - x_1)^2 + (y_2 - y_1)^2 + (z_2 - z_1)^2}\]
And velocity
\[\mathbf{v}(t) = \dot{\mathbf{r}}(t) = \frac{d\mathbf{r}}{dt}\]
Notice these are all in Cartesian co-ordinates, but other co-ordinates can also be useful.

Having said that, let's define space and time:
\begin{itemize}
    \item [Space] is homogeneous (same everywhere) \& isotropic (looks the same everywhere)
    \item [Time] is homogeneous
    \item [\(\implies\)] if there are no forces on a particle, it will stay at rest forever if fully at rest.
    \item [\(\implies\)] in \underline{inertial} frames, \[m\ddot{\mathbf{r}}(t) = \mathbf{f}(\mathbf{r}(t), \mathbf{r}(t))\]
\end{itemize}

\subsection{Forces}
\begin{itemize}
    \item Gravitational
    \item E\&M
    \item ``Nothing else'' (e.g. contact forces come from electrostatic repulsion)
    \item Weak \& Nuclear Forces (and ``higgs force''). Only operate on tiny distances \(\implies\) QM, so these are outside of the classical realm.
\end{itemize}

\section{Main Problem of Mechanics}
Given \(\mathbf{r}\) \& \(\dot{\mathbf{r}}\) @ \(t = t\), find \(\mathbf{r}(t)\) at any \(t_2 < t < t_1\).

Baked into this is the principle of determinism: the ``state'' \((\mathbf{r}, \dot{\mathbf{r}}) @ t = t\) determines state \(\forall t\). Why? Experiment! (in the classical realm, up to the late 19th century).

We'll be revisiting all of this in the new mathematical framework we're about to introduces

\subsection{Many Particles}
\(N\) particles such as
\begin{itemize}
    \item Gas with many atoms
    \item Solar system with many planets
\end{itemize}
\[\implies 3N \textit{coordinates} = \#\text{degrees of freedom (dof)}\]
\(3N\) dof \(q_1,q_2,...,q_{3N}\) can be Cartesian co-ordinates, but don't have to be.

Define \underline{any} \(3N\) numbers \(\{q_i\}\) that define position of all the elements in the system to obtain ``\underline{generalized co-ordinates}''. The state at \(t\) is determined by \(6N\) numbers \(\{q_i, \dot{q_i}\}\). 

If \(N\) particles are Cartesian \(\{q_i\} =(x_1, y_1, z_1, x_2,...)\), physics determined by \(3N\) equations \(m\dot{\dot{q_i}} = f_i\).

We're now going to introduce much more powerful and much more general machinery: the
\subsection{Principle of Least Action}
Also called ``Hamilton's principle'' or ``Lagrangian dynamics''.

For example, consider a simple particle in 1D with potential energy \(U(x)\)
\[\implies f(x) = -\frac{dU}{dt} = -U'(t)\]
We know the potential \(U(x)\), \[T = \frac{1}{2}mv^2 = \frac{1}{2}m\dot{x}^2 \implies \text{total} E = U + T\]
A more fundamental quantity is the Lagrangian \(\mathcal{L}\) or \(\mathcal{Z}\), in this case \(\mathcal{L} = T - U\). This allows us to define the \underline{ACTION} of the particle \[S = \int_{t_1}^{t_2}dt\mathcal{L}(x(t),\dot{x}(t)) = \int_{t_1}^{t_2}\left(\frac{m\dot{x}}{2} - U(x(t))\right)dt\]
\(S\) is a \underline{functional}.

Consider all possible trajectories from \(x_1t_1 \to x_2t_2\): all functions which satisfy \(x(t_1) = x_1, x(t_2) = x_2\). Each \(x(t)\) gives an \(S\).

Why is this useful? We can condense each trajectory, each \(x(t)\), to a single number by plugging it into the integral, which can be quite nasty. But why would I do this? The reason this is incredibly powerful is due to the principle of least action. The principle of least action tells us that the true trajectory that is chosen by the particle is the one that minimizes the action. I throw a ball, the Earth around the sun, whatever this whole system does, it minimizes this action with respect to all possibilities for the trajectory.
\[x_1 = x(t_1), x_2 = x(t_2) \implies x(t) \ \text{minimizes action} \ S = \int_{t_1}^{t_2}dt\mathcal{L}\]
What does this say? Here we're doing something totally different from differential equations: presupposing the endpoints, and finding the path between them, the path of the particles being the minimum of some function. Any deviation from the path will increase \(S\).

How do I formally tell \(x(t)\) minimizes \(S[x]\) (square brackets here mean a functional: something depends on a function). \(S[x]\) is extremal if we have a trajectory \(x(t)\) and if I change it ``slightly'' (add an arbitrary smooth \(\delta x(t)\)), we have \[S[x(t) + \delta x(t)] > S[x(t)] \forall S[x(t)]\]
This is true for any variation, even if the derivative is not small (e.g. ``not smooth''), but if we have a smooth variation, we have
\[S[x(t) - \delta x(t)] - S[x(t)] = O(\delta x^2) \ \text{if} \ x(t) \ \text{extremizes} \ S\]
Analogous to familiar extrema of functions
\[dF = F(y^* + \delta y) - F(y^*) = O(\delta y^2) \iff \left.\frac{dF}{dy}\right|_{y = y^*} = 0\]
Similarly, we have the functional derivative
\[\frac{\delta S[x]}{\delta x(t)} = \lim_{\delta x(t) \to 0}\frac{S[x + \delta x] - S[x]}{S[x]} = 0\]
\[\forall \delta x(t) \to 0,  \forall t \in (t_1, t_2)\]
This is the first variational derivative of \(S[x]\). Note that the above is a function of \(t\), not a number. 
\end{document}
