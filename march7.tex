\documentclass{article}
\usepackage[utf8]{inputenc}

\title{PHY354 Notes: Rigid Body Motion}
\author{Jad Elkhaleq Ghalayini}
\date{March 7 2018}

\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage{cancel}

\newcommand{\lgr}[0]{\mathcal{L}}
\newcommand{\bigo}[0]{\mathcal{O}}
\newcommand{\prt}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\vf}[1]{\mathbf{#1}}
\newcommand{\total}[1]{\frac{d}{dt}\left(#1\right)}

\newtheorem{theorem}{Theorem}
\newtheorem*{claim}{Claim}
\newtheorem*{note}{Note}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}

\begin{document}

\maketitle

Last time, we proved that \(\vf{\Omega}\) does not depend on \((x', y', z')\) origin,
\[\vf{\rho} = \vf{R} + \vf{r}, \Tilde{\vf{R}} = \vf{R} + \vf{a}, \Tilde{\vf{r}} = \vf{r} - \vf{a}\]
From the CM system, we know
\[\tilde{V} = \frac{d\Tilde{\vf{R}}}{dt} = \vf{V}_{cm} + \vf{\Omega} \times \vf{a}\]
equal \(\frac{d\vf{\rho}}{dt}\) for both \(x'\) systems:
\[\frac{d\vf{\rho}}{dt} = \Tilde{\vf{V}} + \Tilde{\vf{\Omega}} \times \Tilde{\vf{r}} = (\vf{V}_{cm} + \vf{\Omega} \times \vf{a}) + \Tilde{\vf{\Omega}} \times \Tilde{\vf{r}}\]
\[= \Tilde{\vf{V}}_{cm} + \vf{\Omega} \times \vf{r} - (\Tilde{\vf{\Omega}} - \vf{\Omega}) \times \vf{a} \implies \vf{\Omega} = \Tilde{\vf{\Omega}}\]
\section{Dynamics of Rigid Bodies}
We can now describe position and velocity of an arbitrary rigid body in a fixed (lab frame) coordinate system. To figure out dynamics, we want the Lagrangian of the COM. From now on, let the origin of \((x', y', z')\) be the CM.

Kinetic energy \[T = \sum_a\frac{m_a\vf{v}_a^2}{2}\] in a body: \(a\) labels particles in the body. Break this down:
\[\vf{v}_a = \vf{V}_{cm} + \vf{\Omega} \times \vf{r}_a\]
\[\implies T = \sum_a\frac{1}{2}m_a(\vf{V}_{cm} + \vf{\Omega} \times \vf{r}_a)^2\]
\[= \sum_a\frac{1}{2}m_a[\vf{V}_{cm}^2 + (\vf{\Omega} \times \vf{r}_a)^2 + \cancel{2\vf{V}_{cm} \times (\vf{\Omega} \times \vf{r}_a)}]\]
since \(\vf{r}_a \cdot (\vf{V} \times \vf{\Omega}), \sum_am_a\vf{r}_a = 0 COM\).
Defining \(\mu = \sum_am_a\), we obtain
\[T = \frac{1}{2}\mu\vf{V}_{cm}^2 + \frac{1}{2}\sum_am_a(\vf{\Omega} \times \vf{r}_a)\cdot(\vf{\Omega} \times \vf{r}_a)\]
Using \((A \times B) \cdot (C \times D) = (AC)(BD) - (AD)(BC)\), we get
\[(\vf{\Omega} \times \vf{r}_a)\cdot(\vf{\Omega} \times \vf{r}_a) = \Omega^2r_a^2 - (\vf{\Omega} \cdot \vf{r}_a)^2\]
\[\implies T = \frac{1}{2}\mu\vf{V}_{cm}^2 + \frac{1}{2}\sum_am_a(\vf{\Omega}^2\vf{r}_a^2 - (\vf{\Omega} \cdot \vf{r}_a)^2)\]
Switch to index notation, write in \((x, y, z)\) and \((x',y', z')\) system (pick one):
\[\vf{r}_a = (r_{a1}, r_{a2}, r_{a3}), \vf{\Omega}^2 = \Omega_i\Omega_i, \vf{\Omega} \cdot \vf{r}_a = \Omega_ir_{ai}\]
\[\implies \omega^2\vf{r}_a^2 - (\vf{\Omega} \cdot \vf{r}_a)^2 = \vf{r}_a^2\Omega_i\Omega_i - (\Omega_ir_{ai})(\Omega_ir_{ai})\]
\[= \vf{r}_a^2\Omega_i\Omega_j\delta_{ij} - \Omega_i\Omega_jr_{ai}r_{aj} = \Omega_i\Omega_j(\vf{r}_a^2\delta_{ij} - r_{ai}r_{aj})\]
\begin{definition}
Define the \underline{INERTIA TENSOR} of the body to be
\[I_{ij} = \sum_am_a(\delta_{ij}\vf{r}_a^2 - r_{ai}r_{aj}) = \int_Vd^3r\rho(\vf{r})(\vf{r}^2\delta_{ij} - r_ir_j)\]
\end{definition}
\(I_{ij}\)is some characteristic of the rigid body, only depends on masses and their distribution, usually calculated in \(x'\) system. Then
\[T = \frac{1}{2}\mu\vf{V}^2 + \frac{1}{2}\Omega_i\Omega_iI_{ij}\]
In \((x', y', z')\) or \((x, y, z)\) space, \(I_{ij}\) is a \(3 \times 3\) \underline{matrix} (rank 2 tensor), just the \(\vf{r}, \vf{v}\) are 3-vectors (rank 1 tensors).
\[I_{ij} = I_{ji}\]
i.e. \(I_{ij}\) is a symmetric tensor. So using linear algebra, we know that symmetric matrices can be diagonalized by an orthogonal transformation. For an \(n \times n\) symmetric matrix, we can find an \(n \times n\) orthogonal matrix when \(OO^T = \mathbb{1}\).
\[I^{diag} = OIO^T, I_{ij}^{diag} = I_i\delta_{ij} = O_{ik}I_{kl}O_{jl}\]
So
\[I_{diag} = \begin{pmatrix} I_1 & 0 & 0 \\ 0 & I_2 & 0 \\ 0 & 0 & I_3 \end{pmatrix}\]
\(I_{1, 2, 3}\) are the PRINCIPLE MOMENTS OF INERTIA (eigenvalues of the inertia tensor). The orthogonal transformation \(O\) corresponds to a change of orientation of the \((x', y', z')\) system such that \(I_{ij}\) is diagonal in that rotated system. The corresponding directions \(\hat{x}', \hat{y}', \hat{z}'\) are called PRINCIPAL AXES OF INERTIA, ie. the eigenvectors of \(I\) CHECK.

For an arbitrary body, we have to perform MATH to find \(O\), \(\hat{x}'\), etc. But if the body has any symmetries, usually easy because Princ. Axes have to respect some symmetry.
\subsection{Example of an ``arbitrary'' (bad) choice of \((x', y', z')\)} 
Consider e.g. a rigid diatomic molecule with \(m_1 = m_2 = m\)).
\[r_{11} = -a\cos\theta, r_{21} = a\cos\theta, r_{12} = -a\sin\theta, r_{22} = a\sin\theta, r_{13} = 0, r_{23} = 0\]
We can then work out \(I_{ij}\):
\[I_{11} = m(a^2 - a^2\cos^2\theta) + m(a^2 - a^2\cos^2\theta) = 2ma^2\sin^2\theta\]
\[I_{12} = I_{21} = -ma^2\cos\theta\sin\theta\]
\[I_{22} = 2ma^2\cos^2\theta\]
\[I_{33} = 2ma^2\]
\[I_{13} = I_{31} = I_{23} = I_{23} = 0\]
\[\implies I = 2ma^2\begin{pmatrix}\sin^2\theta & -\sin\theta\cos\theta & 0 \\ -\sin\theta\cos\theta & \cos^2\theta & 0 \\ 0 & 0 & 1\end{pmatrix}\]
What are the principal axes/moments?
\begin{itemize}

    \item [Math:] find eigenvalues of \(I\): 1, 1, 0. Also
    \[(\sin^2\theta - \lambda)(\cos^2\theta - \lambda) - \sin^2\theta\cos^2\theta = 0\]
    \[= \lambda^2 - \lambda \iff \lambda = 1, 0\]
    \[\implies I = 2ma^2\begin{pmatrix} 0 & 0 & 0 \\ 0 & 1 & 0 \\ 0 & 0 & 1 \end{pmatrix}\]
    eigenvectors are...
    
    \item [Physics:] We have rotational symmetry around the axis \(\to\) choose \(\theta = 0\)
    \[\implies I = 2ma^2\begin{pmatrix} 0 & 0 & 0 \\ 0 & 1 & 0 \\ 0 & 0 & 1 \end{pmatrix}\]
    Line up \((x', y', z')\) basis vectors with rotational symmetry axes.
    \(\implies\) general form of kinetic energy
    \[T = \frac{1}{2}\mu\vf{V}_{cm}^2 + \frac{1}{2}\Omega_i\Omega_jI_{ij}\]
    If \((x', y', z')\) are aligned with principal axes,
    \[T = \frac{1}{2}\mu\vf{V}_{cm}^2 + \frac{1}{2}(I_1\Omega_1^2 + I_2\Omega_2^2 + I_3\Omega_3^2)\]
\end{itemize}
Consider 

\end{document}