\documentclass{article}
\usepackage[utf8]{inputenc}

\title{PHY354 Notes: Rigid Body Motion}
\author{Jad Elkhaleq Ghalayini}
\date{March 5 2018}

\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage{cancel}

\newcommand{\lgr}[0]{\mathcal{L}}
\newcommand{\bigo}[0]{\mathcal{O}}
\newcommand{\prt}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\vf}[1]{\mathbf{#1}}
\newcommand{\total}[1]{\frac{d}{dt}\left(#1\right)}

\newtheorem{theorem}{Theorem}
\newtheorem*{claim}{Claim}
\newtheorem*{note}{Note}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}

\begin{document}

\maketitle

What is a rigid body? Any continuous or discrete collection of mass(es) that does not deform relative to each other. For a continuous mass, we have a density function \(\rho(\vf{r})\) which we can obtain by integrating
\[M = \int_Vd^3r\rho(\vf{r})\]

\section{Kinematics}
How do we describe the position and motion of a rigid body?
\begin{enumerate}
    
    \item Pick an arbitrary point on \(Rc\) and its coordinates tell you the position of the body as a whole
    \begin{itemize}
        \item [\(\implies\)] the CM
        \item [\(\implies\)] 3 coordinates
    \end{itemize}
    It's like a CAD drawing: within a local co-ordinate system defined in relation to the item.
    
    \item Specify orientation: 
    \begin{itemize}
        \item[\(\implies\)] define \((x', y', z')\) co-ordinates with origin at the reference point which moves with the body. This is NOT an inertial frame. We do all our dynamics in the nice inertial lab frame.
        \item [\(\implies\)] we need 6 coordinates to describe the position of a rigid body:
        \begin{itemize}
            \item The position of the reference point \(\vf{R}\)
            \item Rotation of \((x', y', z')\) system with respect to the \((x, y, z)\) system:
            \begin{itemize}
                \item Unit vector (axis) \(\hat{n}\)
                \item Rotation angle
            \end{itemize}
        \end{itemize}
    \end{itemize}

\end{enumerate}
Now let's imagine \(\vf{R}\) is the CM position: let the unit vectors of the \((x', y', z')\) system in the \((x, y, z)\) system be
\[\hat{x}', \hat{y}', \hat{z}'\]
Let an arbitrary point in the body have position vector \(\vf{\rho}\) in the \((x, y, z)\) system and \(\vf{r}\) in the \((x', y', z')\) system
\[\implies \vf{r} = r_1\hat{x}' + r_2\hat{y}' + r_3\hat{z}' \implies \vf{\rho} = \vf{R} + \vf{r}\]
From time \(t\)to time \(t + dt\) the motion of the point consists of
\begin{enumerate}
    \item CM displacement of \(\vf{R}\)
    \item Rotation: the body (and the \((x', y', z')\)) system) will rotate by angle \(d\phi\) around axis \(\hat{n}\)
    \begin{itemize}
        \item [\(\to\)] infinitesimal rotation: \(d\vf{\phi} = d\phi\hat{n}\)
    \end{itemize}
    \item [\(\implies\)] \[d\vf{\rho} = d\vf{R} + d\vf{r} = d\vf{R} + d\vf{\phi} \times \vf{r}\]
    \item [\(\implies\)] \[\frac{d\vf{\rho}}{dt} = \frac{d\vf{R}}{dt} + \vf{d\vf{\phi}}{dt} \times \vf{r}\]
    E.g. the velocity of \(\vf{\rho}\) in \((x, y, z)\) is equal to the velocity \(\vf{V}\) of \(\vf{R}\) in \((x, y, z)\) plus \(\vf{\Omega}\), the angular velocity of the center of motion, crossed with \(\vf{r}\). If we express \(\frac{d\vf{\rho}}{dt}\) in the \((x', y', z')\) system, \(\vf{r}\) does not change. We have to be careful to express this term in terms of the \(\hat{x}', \hat{y}', \hat{z}'\) unit vectors.
    \[\implies \vf{v} = \vf{V} + \vf{\Omega} \times \vf{r}\]
    Example: a disk rotating with angular velocity \(\omega\)
    \begin{itemize}
        \item [\(\implies\)] \(\vf{r} = A(1, 0, 0) \text{ in } (x', y', z') = A\hat{x}'\)
        in the \((x, y, z)\) system.
        \[\hat{x}' = (\cos\omega t, \sin\omega t, 0), \hat{y} = (-\sin\omega t, \cos\omega t, 0), \hat{z} = (0, 0, 1)\]
    \end{itemize}
    So in the \((x, y, z)\) system,
    \[\vf{\rho} = \vf{r} = A(\cos\omega t, \sin\omega t,0)\]
    \[\vf{v} = \frac{d\vf{\rho}}{dt} = A\omega(-\sin\omega t, \cos\omega t)\]
    This is equal to
    \[= \omega(0, 0, 1) \times (A\cos\omega t, B\sin\omega t, 0) = \vf{\Omega} \times \vf{r} = (\omega\hat{z}')(A\hat{x}')\]
\end{enumerate}
What if the origin of \((x', y', z')\) is not the CM? Consider a point of interest with position in the \((x, y, z)\) frame given by the vector \(\vf{\rho}\), and let \(\vf{R}\) be the vector to the COM and \(\Tilde{\vf{\rho}}\) be some other choice of reference point. Then, we have \(\vf{r}, \Tilde{\vf{r}}\) such that
\[\vf{\rho} = \vf{R} + \vf{r} = \Tilde{\vf{R}} + \Tilde{\vf{r}}\]
Let \(\vf{a}\) be such that
\[\vf{R} = \Tilde{\vf{R}} + \vf{a}, \vf{r} = \Tilde{\vf{r}} - \vf{a}\]
Then
\[d\vf{\rho} = d\vf{r} + d\vf{\phi} \times \vf{r} = d\Tilde{\vf{R}} + (d\vf{\phi} \times \Tilde{\vf{r}}) - d\vf{\phi} \times \vf{a}\]
\[(d\Tilde{\vf{R}} - d\vf{\phi} \times \vf{a}) + d\vf{\phi} \times \Tilde{\vf{r}}\]
\[= \Tilde{\vf{V}} + (\Tilde{\vf{\Omega}} \times \Tilde{\vf{r}})\]
\[\implies \Tilde{\vf{\Omega}} = \vf{\Omega}, \Tilde{\vf{V}} = \vf{V} - \vf{\Omega} \times \vf{a}\]
\end{document}