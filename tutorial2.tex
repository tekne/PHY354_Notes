\documentclass{article}
\usepackage[utf8]{inputenc}

\title{%
PHY354 Tutorial 2 Notes \\
\large Gunjan Lakhtani, TUT0101BB}
\author{Jad Elkhaleq Ghalayini}
\date{January 22 2018}

\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage{cancel}

\newtheorem{theorem}{Theorem}
\newtheorem*{claim}{Claim}

\begin{document}

\maketitle

\begin{theorem}{Noether's Theorem:}
For every continuous symmetry in the Lagrangian there is a corresponding quantity conserved
\end{theorem}

Interesting example: there is a hidden quantity conserved in Kepler's problem called the Laplace-Runge-Lenz vector, coming from a hidden higher dimensional symmetry.

Since you're learning from Landau \& Lifschitz, an interesting article is that of Borris Ioffe.

\begin{itemize}

    \item [Landau, 2.1)] Suppose you have the \(xy\) plane and in the first half of the plane (2nd and 3rd quadrants) we have potential \(U_1\), and at the boundary it changes to \(U_2\), giving a step function. For simplicity, let's say the potential is only a function of \(x\) and does not vary with \(y\). The particle comes in from the left with velocity \(v_1\) and angle \(\theta_1\) with respect to the \(x\) axis and leaves with velocity \(v_2\) and angle \(\theta_2\). We write
    \[\mathcal{L} = \frac{1}{2}mv^2 - U(x) = \frac{1}{2}m(\dot{x}^2 + \dot{y}^2) - U(x); U(x) = \left\{\begin{array}{c}U_1, x < 0 \\ U_2, x > 0 \end{array}\right.\]
    Using Noether's theorem, wehave
    \[p_y = \text{ constant }\]\[\frac{\partial\mathcal{L}}{\partial y} - \frac{d}{dt}\left(\frac{\partial\mathcal{L}}{\partial y}\right) = 0\]
    \[\implies \frac{\partial\mathcal{L}}{\partial y} = \text{ const } = \dot{y} = v_1\sin\theta_1 = v_2\sin\theta_2\]
    For the \(x\)-direction,
    \[\frac{\partial\mathcal{L}}{\partial x} - \frac{d}{dt}\left(\frac{\partial\mathcal{L}}{\partial\dot{x}}\right) = 0\]
    \[E = \dot{x}\frac{\partial\mathcal{L}}{\partial\dot{x}} + \dot{y}\frac{\partial\mathcal{L}}{\partial\dot{y}} - \mathcal{L}\]
    \[= \frac{1}{2}m(\dot{x}^2 + \dot{y}^2) - U(x) = \text{ const }\]
    So we write
    \[\frac{1}{2}mv_1^2 + U_1 = \frac{1}{2}mv_2^2 + U_2 \implies \frac{1}{2}m(v_1^2 - v_2^2) = U_1 - U_2\]
    \[\implies \frac{1}{2}m\left(\frac{v_1^2}{v_2^2} - 1\right) = \frac{U_1 - U_2}{v_1^2}\]
    \[\frac{v_1}{v_2} = \sqrt{1 + \frac{2}{mv_1^2}(U_1 - U_2)} = \frac{\sin\theta_1}{\sin\theta_2}\]
    This gives the law of refraction for a \underline{massive} particle. Think about what the difference between this case and light is.
    
    \item Find the Lagrangian of a particle constrained to the surface of a cone making angle \(\alpha\) with the \(z\) axis:
    \begin{figure}
        \centering
        \begin{tikzpicture}
            \draw[->](0, 0, 0) -- (0, 0, 2) node[anchor = north east]{y};
            \draw[->](0, 0, 0) -- (0, 2, 0) node[anchor = south]{z};
            \draw[->](0, 0, 0) -- (2, 0, 0) node[anchor = west]{x};
            \draw(0, 0, 0) -- (1, 2, 0);
            \draw(0, 0, 0) -- (-1, 2, 0);
        \end{tikzpicture}
        \caption{Problem 2}
        \label{fig:my_label}
    \end{figure}
    We use cylindrical co-ordinates, giving
    \[x = r\cos\theta, y = r\sin\theta, z = \frac{r}{\tan\alpha}\]
    We have, in general
    \[\dot{x}^2 + \dot{y}^2 + \dot{z}^2 = \dot{r}^2 + r^2\dot{\theta}^2 + r^2\sin^2\theta\dot{\phi}^2\]
    In this case, we write
    \[T = \frac{1}{2}m(\dot{x}^2 + \dot{y}^2 + \dot{z}^2) = \frac{1}{2}m\left(\dot{r}^2 + r^2\dot{\theta}^2 + \frac{\dot{r}^2}{\tan\alpha}\right)\]
    \[U = mgz = mg\frac{r}{\tan\alpha} \implies \mathcal{L} = T - U = \frac{1}{2}m\left(\frac{\dot{r}^2}{\sin^2\alpha} + r^2\dot{\theta}^2\right) - mg\frac{r}{\tan\alpha}\]
    \[p_\theta = \frac{\partial\mathcal{L}}{\partial\dot{\theta}} = \text{ conserved } = mr^2\dot{\theta} = l\]
    Using the Euler-Lagrange equations, we can write down the EOM (steps omitted, as differentiation does not shine light on anything):
    \[\frac{m\dot{r}}{\sin^2\alpha} = mr\dot{\theta}^2 - \frac{mg}{\tan\alpha}\]
    \[\ddot{r} = r\sin^2\alpha - g\sin\alpha\cos\alpha = \frac{\sin^2\alpha l^2}{r^3m^2} - g\sin\alpha\cos\alpha\]
    Consider \(r = r_0 = \text{ const } \implies \dot{r}_0 = \ddot{r}_0 = 0\)
    \[r_0 = \left(\frac{l^2\tan\alpha}{m^2g}\right)^{1/3}, \dot{\theta} = \frac{g}{r_0\tan\alpha}\]
    
    \item The geodesic equation:
    \[\mathcal{L} = \frac{m}{2}g_{ij}(q)\dot{q}^i\dot{q}^j\]
    \[\frac{\partial\mathcal{L}}{\partial q^l} - \frac{d}{dt}\left(\frac{\partial\mathcal{L}}{\partial\dot{q}^l}\right)\]
    If you can solve it, you should get something of the form
    \[\ddot{q}^i - \frac{g^{lk}}{2}\left(\frac{\partial g_{il}}{\partial q^j} + \frac{\partial g_{jl}}{\partial q^i} - \frac{\partial g_{ij}}{\partial q^l}\right)\dot{q}^i\dot{q}^j\]
    
\end{itemize}

\end{document}