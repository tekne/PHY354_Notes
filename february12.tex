\documentclass{article}
\usepackage[utf8]{inputenc}

\title{PHY354 Notes: The Hamiltonian}
\author{Jad Elkhaleq Ghalayini}
\date{February 11 2018}

\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage{cancel}

\newcommand{\lgr}[0]{\mathcal{L}}
\newcommand{\bigo}[0]{\mathcal{O}}
\newcommand{\prt}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\vf}[1]{\mathbf{#1}}
\newcommand{\total}[1]{\frac{d}{dt}\left(#1\right)}
\newcommand{\hml}[0]{H}
\newcommand{\energy}[0]{\mathcal{E}}

\newtheorem{theorem}{Theorem}
\newtheorem*{claim}{Claim}
\newtheorem*{note}{Note}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}

\begin{document}

\maketitle

\section{Legendre Transform}

Consider a function \(y = f(x)\) such that \(f''(x) > 0\). You can ``pick out''a particular value of \(x\) by defining \(F(x, p) = xp - f(x)\). Then \(F(x, p)\) has a maximum with respect to \(x\) for a given value of \(p\):
\[0 = \prt{F}{x} = p - f'(x) \implies p = f'(x(p))\]
Invert this to define \(x = x(p)\).

\begin{definition}
The Legendre transform of \(f(x)\) with \(f''(x) > 0\) is
\[g(p) = F(x(p), p) = x(p)p - f(x(p))\]
where \(x(p)\) is defined by \(p = f'(x(p))\).
\end{definition}
\(g(p)\) ``contains the same information'' as \(f(x)\) in some way.
\begin{claim}
\begin{enumerate}
    \item \(g(p)\) also obeys \(g''(p) > 0\)
    \item The Legendre Transform is an ``involution'' (applied twice gives trivial transform, like a reflection, rotation by \(\pi\), etc), i.e.
    \[g(p) \to f(x), \text{ ``(Legendre)'' } = 1\]
\end{enumerate}
\end{claim}

\begin{proof}
\begin{enumerate}

    \item \[g'(p) = \frac{d}{dp}(xp - f(x)) = x'(p)p - f'(x(p))x'(p) = 0\] since \(f'(x(p)) = p\), which implies that
    \[g'(p) = x(p), g''(p) = x'(p) \implies g''(p) = \frac{1}{f''(x(p))} > 0\]
    But \(p = f'(x(p))\), so
    \[\frac{dp}{dp} = 1 = \frac{d}{dp}f'(x(p)) = f''(x(p))x'(p) \implies x'(p) = [f''(x(p))]^{-1}\]
    
    \item Find \(h(z)\), the Legendre transform of \(g(p)\): consider \(\ell(z, p) = pz - g(p)\). Solve for \(p(z)\) by requiring 
    \[\prt{\ell}{p} = 0 \implies z = g'(p) \text{ defines }  p(z) \implies p(z)z - g(p(z)\]
    \[= p(z)z - (p(z)x(p(z)) - f(x(p(z)))) = p(z)(z - x(p(z))) + f(x(p(z)))\]
    But whe know that
    \[z = g'(p) = \frac{d}{dp}\left(px(p) - f(x(p))\right) = x(p) + \cancel{p(z)x'(p) - f'(x)x} \implies x(p) = z\]
    \[\implies h(z) = \cancel{p(z)(z - z)} + f(z)\]
    
\end{enumerate}
\end{proof}

\section{The Hamiltonian}

Consider a Lagrangian of one variable
\[\lgr(q, \dot{q}, t)\]
(Don't assume the system is ``conservative'', i.e. allow \(\lgr\) to explicitly depend on \(t\))
Then
\[\hml(p, q, t) = \text{ Legendre transform of } \lgr(q, \dot{q}, t)\]
(with respect to \(\dot{q}\), i.e.\(q, t\) staying along for the ride.

We have
\[\hml(p, q, t) = p\dot{q} - \lgr(q, \dot{q}, t)\]
where
\[p = \prt{\lgr}{\dot{q}} \text{ defines } \dot{q} = \dot{q}(q, p, t)\]
Note that \(\hml\) is the TOTAL ENERGY \(\hml = T + U\) in the system.

What does this function \(\hml\) tell us?
\[d\hml(pqt) = \prt{\hml}{p}dp + \prt{\hml}{q}dq + \prt{\hml}{t}dt = d(p\dot{q} - \lgr(q, \dot{q}, t))|_{p = \prt{\lgr}{\dot{q}}}\]
\[= dp\left(\dot{q} + p\prt{\dot{q}}{p} - \prt{\lgr}{\dot{q}}\prt{\dot{q}}{p}\right) = dp\left(\dot{q} + p\prt{\dot{q}}{t} - \prt{\lgr}{\dot{q}}\prt{\dot{q}}{t}\right)dt\]
\[= \dot{q}dp - \prt{\lgr}{q}dq - \prt{\lgr}{t}dt\]
\[\implies \prt{\hml}{p} = \dot{q}, \prt{\hml}{q} = -\prt{\lgr}{q} = -\dot{p}, \prt{\hml}{t} = -\prt{\lgr}{t}\]
Hence if \(q, \dot{q}\) obey the EL equations, then \(p, q\) obey
\[\dot{q} = \prt{\hml}{p}(p, q, t), \dot{p} = -\prt{H}{q}(p, q, t)\]
- Hamilton's Equations of Motion. Furthermore,
\[\prt{\hml(p, q, t)}{t} = -\prt{\lgr(q, \dot{q}(q, p, t), t)}{t}\]
if there is explicit time dependence.

So we transform one 2nd order differential equation into 2 1storder differential equations, which are sometimes easier to solve, and make deep connections with QM and statistical mechanics.

\subsection{Many variables}
This generalizes as follows:
\[\lgr = \lgr(q_i, \dot{q}_i, t) \implies \hml(p_i, q_i, t) = \left(\sum_ip_i\dot{q}_i\right) - \lgr(q_i, \dot{q}_i, t)\]
where
\[p_i = \prt{\lgr(q_i, \dot{q}_i, t)}{\dot{q}_i}\]
is solved to define \(\dot{q}_i = \dot{q}_i(p, q, t)\)
and the Euler Lagrange equations are equivalent to
\[\dot{p}_i = -\prt{\hml}{q_i}, \dot{q}_i = \prt{H}{p_i}\]

\section{Conservation Laws?}
\begin{itemize}
    \item Conserved quantitites occur when \(\hml(p, q, t)\) is independent of some \(q_i\), then
    \[\dot{p}_i = -\prt{H}{q_i} = 0 \implies p_i = \text{ const, conserved!}\]
    \item What about conserved \(q_i\)'s? NO! This would imply that
    \[\prt{H}{p_i} = 0\]
    but \(H\) must be concave: \(\frac{\partial^2\hml}{\partial p_i^2} > 0\)!
    \item Energy conservation? \(\energy = \hml \implies \prt{\hml}{t} = 0 \iff\) energy is conserved
\end{itemize}

\section{Back to \(\lgr\)?}
By the involution property of Legendre transforms, given \(H(p, q)\), we can get \(\lgr(q, \dot{q})\) via another Legendre transform.
\[\lgr(q, \dot{q}) = \dot{q}p - H(p, q)\]
where \(\dot{q} = \prt{H}{p}\) defines \(p = p(q)\).
\begin{note}
Since \(\dot{q}\) was replaced by \(p\) to get \(\hml\), we call \(p\) ``the \underline{canonical} momentum \underline{conjugate} to \(\dot{q}\)''
\end{note}

\section{Phase Flow \(\to\) Poisson Brackets \(\to\) QM!}
For \(N\) particles in 3D, we have \(\{q_i(t), p_i(t)\} \to\) a 6D space called \underline{PHASE SPACE}!

\end{document}