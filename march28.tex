\documentclass{article}
\usepackage[utf8]{inputenc}

\title{PHY354 Notes: Non Intertial Frames and Spinning Tops}
\author{Jad Elkhaleq Ghalayini}
\date{March 28 2018}

\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage{enumitem}
\usepackage{cancel}

\newcommand{\lgr}[0]{\mathcal{L}}
\newcommand{\bigo}[0]{\mathcal{O}}
\newcommand{\prt}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\vf}[1]{\mathbf{#1}}
\newcommand{\total}[1]{\frac{d}{dt}\left(#1\right)}

\newtheorem{theorem}{Theorem}
\newtheorem*{claim}{Claim}
\newtheorem*{note}{Note}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}

\begin{document}

\maketitle

\section{Last Time}
We discussed the equations for the heavy symmetric top.

\section{Special case: Uniform Precession (``Circular Orbit'')}
Can you have precession without mutation, i.e. \(\dot{\theta} = 0\) and \(\dot{\phi}\) constant? Yes: we need 
\[u_{\min} = u_{\max} \implies f(u_0) = 0, f'(u_0) = 0\]
which gives equations
\begin{enumerate}[label=(\Roman*)]
    \item \((1 - u_0^2)(\alpha - u_0) - (b - au_0)^2 = 0\)
    \item \(-2u_0(\alpha - \beta u_0) - \beta(1 - u_0^2) + 2a(b - au_0) = 0\)
\end{enumerate}
allowing us to solve for \(\alpha, \beta\). This gives us, in particular
\[0 = \text{ (II) } + \frac{2u_0}{1 - u_0^2}I \implies U = \beta(1 - u_0^2) - \frac{2u_0}{1 - u_0^2}(b - au_0^2)^2 + 2a(b - au_0)\]
Use
\[\dot{\phi} = \frac{b - au}{1 - u^2} \implies \frac{1}{2}\beta = u_0\dot{\phi}^2 + 2a\dot{\phi}\]
Substitute
\[\alpha = \frac{I_3}{I_1}\Omega_3, \beta = \frac{2mg\ell}{I_1} \implies mg\ell = \dot{\phi}(I_3\Omega_3 - \dot{\phi}I_1\cos\theta_0)\]
\[\implies \dot{\phi} = \frac{I_3\Omega_3}{2I_1\cos\theta}\left(1 \pm \sqrt{1 - \left(\frac{\Omega_{\min}}{\Omega_3}\right)}\right)\]
\[\implies \dot{\phi} = \dot{\phi}_{slow} \text{ or } \dot{\phi}_{fast}\]
is constant, implying we have to give tops those values to get uniform procession. If \(\Omega_3 < \Omega_{\min}\), top falls over. 

\section{Non-Inertial Frames}
So far, we always worked in inertial reference frames. But non-inertial frames (NIF) are familiar and common. You feel extra weight in an elevator, get pushed backwards in a car and feel a merry-go-round pulling you outwards. Every experiment done on Earth is to a degree in a non-inertial frame, because it rotates about its axis and around the sun. All the mathematical formalism we developed to work in the body-fixed frame should just transfer right over.

The big question:
\subsection{What is \(\lgr\) in a non-inertial frame?}
We will obtain the general form in two steps:
\begin{itemize}
    
    \item [Step 1:] The translational motion of the \((x', y', z')\) system given by \(\vf{R}(t), \dot{\vf{R}}(t) = \dot{\vf{V}}(t)\) is \underline{PRESCRIBED}. \(x', y', z' || x, y, z\) 
    
    \item [Step 2:] We now have an \((x'', y'', z'')\) system with the same origin as \((x', y', z')\) but rotating with some prescribed angular velocity \(\vf{\Omega}(t)\).
    
\end{itemize}
Example: if the sum is taken as an inertial \((x, y, z)\) frame, then the Earth's center is \((x', y', z')\) and a person standing on the Earth is \((x'', y'', z'')\). This is all just a change in coordinates: we know how to do them
\[\lgr \to \lgr' \to \lgr''\]
\begin{itemize}

    \item [Step 1:] Imagine we have
    \[\lgr = \frac{1}{2}m\vf{v}^2 - U(\vf{r})\]
    in \((x, y, z)\), i.e. a single body in an external potential.
    \[\implies \vf{r}' = \vf{r} - \vf{R}(t), \vf{v}' = \vf{v} - \vf{V}(t)\]
    giving
    \[\lgr' = \lgr(\vf{r}(\vf{r}'), \vf{v}(\vf{v}')) = \frac{1}{2}m(\vf{v} + \vf{V}(t))^2 + U(\vf{r}' + \vf{R}(t))\]
    \[= \frac{1}{2}m\vf{v}'^2 + m\vf{v}'\cdot\vf{V}(t) + \cancel{\frac{1}{2}m\vf{V}(t)^2} - U(\vf{r}' + \vf{R}(t))\]
    (since \(\frac{1}{2}m\vf{V}(t)^2\) doesn't depend on \(\vf{r}\) or \(\vf{v}\)). We have
    \[m\vf{v}'\cdot\vf{V}(t) = \cancel{m\total{\vf{r}'\cdot\vf{V})}} - m\vf{r}'\cdot\frac{d\vf{V}}{dt}\]
    \[\implies \lgr' = \frac{1}{2}m\vf{v}^2 - U(\vf{r}' + \vf{R}(t)) - m\vf{r}'\cdot\frac{d\vf{V}}{dt}\]
    The EOM after step 1 are:
    \[m\frac{d\vf{v}'}{dt} = m\vf{a}' = -\prt{U(\vf{r} + \vf{R}(t))}{\vf{r}'} - m\frac{d\vf{V}}{dt}\]
    like a uniform field force (\(\propto m \iff\) gravity) \underline{opposite} to frame acceleration.
    
    \item [Step 2:] To make this easier, imagine that
    \begin{enumerate}[label=(\alph*)]
        
        \item There is more than one particle in \(x'\) and \(U\) depends on \(|\vf{r}_i - \vf{r}_j| = |\vf{r}_i' - \vf{r}_j'|\)
        
        \item \(U\) is independent of orientation of \((x'', y'', z'')\) w.r.t. \((x', y', z')\) i.e. central potential with center @ \(\vf{R}(t)\)
        
    \end{enumerate}
    We could easily examine more general cases, but that gets messy, so we only do so when needed.
    
    The coordinate transformation is familiar from rigid bodies:
    \[\hat{x}_\alpha'' = R_{\alpha\beta}\hat{x}_\beta' \to r_i' = (R^{-1})_{ij}r_j''\]
    \[v_i' = \total{(R^{-1})_{ij}r_j''} = \frac{dR^{-1}_{ij}}{dt}r_j' + R_{ij}^{-1}\frac{dr_j''}{dt}\]
    \[\implies \vf{v} = \vf{\Omega} \times \vf{r}'' + R^{-1}\vf{v}''\]
    So
    \[\lgr' = \frac{1}{2}m\vf{v}'^2 - m\vf{r}'\cdot\frac{d\vf{V}}{dt} - U(|\vf{r}|)\]
    \[\implies \lgr'' = \frac{1}{2}m(\vf{v}'' + \vf{\Omega} \times \vf{r}'')^2 - m\vf{v}''\cdot\frac{d\vf{V}}{dt} - U(|\vf{r}''|)\]

\end{itemize}

\end{document}