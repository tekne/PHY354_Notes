\documentclass{article}
\usepackage[utf8]{inputenc}

\title{PHY354 Notes: Orbits}
\author{Jad Elkhaleq Ghalayini}
\date{February 28 2018}

\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage{cancel}

\newcommand{\lgr}[0]{\mathcal{L}}
\newcommand{\bigo}[0]{\mathcal{O}}
\newcommand{\prt}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\vf}[1]{\mathbf{#1}}
\newcommand{\total}[1]{\frac{d}{dt}\left(#1\right)}
\newcommand{\pois}[2]{\{#1, #2\}}

\newtheorem{theorem}{Theorem}
\newtheorem*{claim}{Claim}
\newtheorem*{note}{Note}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}

\begin{document}

\maketitle

\begin{claim}
Poisson brackets are preserved under canonical transformations
\end{claim}
\begin{proof}
Consider \(f(p, q), g(p, q)\).
\[\pois{f}{g} = \prt{f}{q}\prt{g}{p} - \prt{f}{p}\prt{g}{q} = \begin{pmatrix} \prt{f}{q} & \prt{f}{p} \end{pmatrix} \begin{pmatrix} 0 & 1 \\ -1 & 0 \end{pmatrix}\begin{pmatrix} \prt{g}{q} \\ \prt{g}{p} \end{pmatrix}\]
It's easy to check
\[\begin{pmatrix} \prt{g}{q} \\ \prt{g}{p} \end{pmatrix} = \mathcal{J}^T\begin{pmatrix} \prt{g}{Q} \\ \prt{q}{P} \end{pmatrix}\]
(similarly for \(f\))
\[\implies \pois{f}{g} = \begin{pmatrix} \prt{f}{Q} & \prt{f}{P} \end{pmatrix}\mathcal{J}J\mathcal{J}^T\begin{pmatrix} \prt{g}{Q} \\ \prt{q}{P} \end{pmatrix}\]
\end{proof}
So if we have a coordinate transformation, then
\begin{itemize}
    \item [] Canonical
    \item [\(\iff\)] Preserves Hamilton's equations
    \item [\(\iff\)] \(J = \mathcal{J}J\mathcal{J}^T\)
    \item [\(\iff\)] \(\pois{Q_i}{Q_j} = \pois{P_i}{P_j} = 0, \pois{Q_i}{P_j} = \delta_{ij}\)
\end{itemize}
Examples:
\begin{enumerate}

    \item Let \(Q_i = p_i, P_i = -q_i\)
    \[\implies \mathcal{J} = \begin{pmatrix} 0 & 1 \\ -1 & 0 \end{pmatrix}\]
    \[\mathcal{J}J\mathcal{J}^T = J\]
    Note: \(\frac{\partial^2H}{\partial p^2} > 0\) is no longer satisfied: that's OK (see notes)!
    
    \item SHO:
    \[H = \frac{p^2}{2m} + \frac{1}{2}m\omega^2q^2\]
    \[\dot{p} = -\prt{H}{q} = -m\omega^2q, \dot{q} = \prt{H}{p} = \frac{p}{m}\]
    Rescaling:
    \[Q = \sqrt{mq}q, P = \frac{p}{\sqrt{mw}}\]
    \[\implies H = \frac{\omega}{2}(Q^2 + B^2)\]
    ...a circle! Nice and simple. We can go one step further:
    \[I = \frac{1}{2}(P^2 + Q^2), Q = \sqrt{2I}\sin\theta, P = \sqrt{2I}\cos\theta, \tan\theta = \frac{Q}{P}\]
    \[\implies H = I\omega\]
    \[\dot{\theta} = \prt{H}{I} = \omega, \dot{H} = -\prt{H}{\theta}\]
    That is, phase space can be made trivial if the system is INTEGRABLE.
    
\end{enumerate}

\section{Integrable Systems of \(H\) Formalism}
If system is integrable, we can find coordinate transformations
\[(q_i, p_i) \to (Q_i, I_i)\]
s.t. \(H = H(I,...,I_n)\) not dependent on \(\theta_i\). \(I_i\) are conserved quantities,
\[\dot{\theta}_i = \prt{H}{I_i} = \omega_i(I_1,...I_n) \implies \theta_i = \omega_it\]
These are called \underline{ACTION ANGLE VARIABLES}.

\subsection{Liouville-Arnold Theorem}
Any system which has \(n\) degrees of freedom and at least \(n\) \underline{mutually} \underline{Poisson-commuting} integrals of motion
\[I_i, (\pois{I_i}{I_j} = 0)\]
is INTEGRABLE and can be written in terms of Action-Angle variables.

Example: 2 body problem with a central potential. We have \(n = 3\) of \(r, \theta, \phi\) for \(U(r)\) (or \(\frac{\alpha}{r}\)), we have \(E = H, \vf{M}\) (and \(\vf{A}\) in the \(\frac{\alpha}{r}\) case). But
\[E, M^2, M_z\]
are Poisson commutative \(\implies\) INTEGRABLE!

\subsection{Noether's Theorem on \(H\) formalism}
Consider an infinitesimal co-ordinate transformation
\[q_i \to Q_i = q_i + \epsilon F_i(q, p)\]
\[p_i \to P_i = p_i + \epsilon E_i(q, p)\]
Where \(E_i, F_i\) are arbitrary functions, noting that \(E_i\) is NOT energy.

We can write the block matrix
\[\mathcal{J} = \begin{pmatrix}
    \delta_{ij} + \epsilon\prt{F_i}{q_j} & \epsilon\prt{F_i}{p_i} \\ \epsilon\prt{E_i}{q_i} & \delta_{ij} + \epsilon\prt{E_i}{p_j}
\end{pmatrix}\]
Then
\[\mathcal{J}J\mathcal{J}^T = \begin{pmatrix}
0 & \delta_{ij} + \epsilon[\prt{F_i}{q_j} + O(\epsilon^2) \\ -\delta_{ij} - \epsilon[\prt{F_i}{q_j} + \prt{E_i}{p_i}] + O(\epsilon^2) & 0
\end{pmatrix}\]
\[\implies \text{ canonical if } \prt{F_i}{q_j} = -\prt{E_i}{p_j}\]

This is true if I choose
\[F_i = \prt{G}{p_i}, E_i = -\prt{G}{q_i}\]
for some \(G(\{q_i,p_i\})\) - the ``GENERATOR'' of the transformation. 
Notice how this looks sorta Hamiltonian-like...

\subsubsection{Physical Significance of \(G\)}
\begin{itemize}

    \item Take \(G = p_k\) for some \(k\)
    \[\implies F_i = \delta_{ik}, E_i = 0\]
    \[\implies p_i \to p_i, q_i \to q_i + \epsilon\delta_{ik}\]
    i.e. infinitesimal shift in \(k\)-direction!
    
    \item \(G = M_z\) (\(z\) component of \underline{total} angular momentum).
    
    In spherical/polar coordinates, \(\phi\) is the momentum conjugate of \(M_z\) i.e. \(p_\phi = M_z\)
    \[\implies F_i = \prt{p_i}{p_\phi} = \delta_{i\phi}, E_i = 0\]
    \[\implies \text{ shift in } \phi \implies \text{ rotation about } z\text{-axis}\]
    
    \item \(G = E = H\), then 
    \[F_i = \prt{H}{p_i} = \dot{q}_i, \ E_i = -\prt{H}{q_i} = \dot{p}_i\]
    so then
    \[q_i(t) \to q_i(t) + \epsilon\dot{q}_i(t) = q_i(t + \epsilon)\]
    \[\implies \text{ time translation } \implies \text{ TIME EVOLUTION = CANONICAL TRANSFORMATION}\]
    
\end{itemize}

\subsubsection*{Interesting:}
Noether's theorem \(\implies\) symmetry action doesn't change action \(\implies\) conserved quantity \(\implies\) generates a canonical transformation \(\implies\) can I get back to the symmetry?

In order for a canonical transformation to represent a symmetry operation it must leave the Hamiltonian unchanged. So, under
\[q_i \to q_i + \epsilon F_i, p_i \to p_i + \epsilon E_i\]
\[\implies \delta H = \prt{H}{q_i}\delta q_i + \prt{H}{p_i}\delta p_i = \epsilon[\prt{H}{h_i}\prt{G}{p_i} = \prt{H}{p_i}\prt{G}{q_i}] + O(\epsilon^2)\]
\[= \epsilon\pois{H}{G}\]
But \(\frac{dG}{dt} = \cancel{\prt{G}{t}} + \pois{G}{H}\).
So if \(G\) is conserved, \([G, H] = 0\)
\(\iff\) conserved quantities generate canonical transformations in which \(H\)is invariant and vice versa.


\end{document}