\documentclass{article}
\usepackage[utf8]{inputenc}

\title{PHY354 Notes: Orbits}
\author{Jad Elkhaleq Ghalayini}
\date{February 7 2018}

\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage{cancel}

\newcommand{\lgr}[0]{\mathcal{L}}
\newcommand{\bigo}[0]{\mathcal{O}}
\newcommand{\prt}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\vf}[1]{\mathbf{#1}}
\newcommand{\total}[1]{\frac{d}{dt}\left(#1\right)}

\newtheorem{theorem}{Theorem}
\newtheorem*{claim}{Claim}
\newtheorem*{note}{Note}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}

\begin{document}

\maketitle

Last time, we discussed how scaling \(\lgr\) does not change the EOM. If \(\vf{x}(t)\) is a solution of the EOM, so is
\[\vf{x}'(t) = \alpha \vf{x}(\beta t), \beta = \alpha^{1 - k/2}\]
So we have the transformation
\[\vf{x} \to \vf{x}' = \alpha \vf{x}, t \to t' = \beta t\]
giving typical scaling for distances and times
\[\frac{\bigo A'}{\bigo A} = \alpha, \frac{\bigo B'}{\bigo B} = \beta \implies \frac{\bigo A'}{\bigo A} = \left(\frac{\bigo B'}{\bigo B}\right)^{\frac{2}{2-k}}\]
More generally,
\[\frac{l'}{l} = \left(\frac{t'}{t}\right)^{1 - k/2}\]
Similarly,
\[v \propto \frac{l}{t} \implies \frac{v'}{v}  = \frac{l'}{l}\frac{t}{t'} = \left(\frac{l'}{l}\right)^{\frac{k}{2}}\]
\[E = T + U \propto v^2 \implies \frac{E'}{E} \propto \left(\frac{v'}{v}\right)^2 = \left(\frac{l'}{l}\right)^k\]
\[M_i \propto lv \implies \frac{M_i'}{M_i} = \frac{l'}{l}\frac{v'}{v} = \left(\frac{l'}{l}\right)^{1 + k}\]
We can now quickly understand a lot of neat things
\begin{enumerate}
    
    \item Consider a harmonic oscillator
    \[U(x) \propto x^2 \implies U(x) \text{ is homogeneous of degree } 2\]
    For time consider trajectories
    \[\frac{t'}{t} = \left(\frac{l'}{l}\right)^{1 - k/2} = 1\]
    implying period is independent of amplitude
    
    \item Consider a constant force \(U = -Fx\) (homogeneous \(E\) field with gravity). Clearly \(k = 1\), so
    \[\frac{t'}{t} = \left(\frac{l'}{l}\right)^{1 - k/2} = \sqrt{\frac{l'}{l}}\]
    \[\implies \text{ time to fall } \propto \sqrt{\text{height}}\]
    
    \item Consider a central force \(U \propto \frac{1}{r} \implies k = -1\).
    \[\frac{t'}{t} = \left(\frac{l'}{l}\right)^{3/2} \implies (\text{\# of periods})^2 = (\text{ratio of radii})^3\]
    aka. Kepler's third law!
    
\end{enumerate}

Another application of homogeneous functions is the
\section{Virial Theorem}
We know kinetic energy is an HF of degree 2 in \(\vf{v}\),
\[T = \sum_a\frac{m_a}{2}\vf{v}_a^2\]
By Euler's Theorem,
\[\sum_a\prt{T}{\vf{v}_a}\cdot\vf{v}_a = 2T \implies 2T = \sum_a\vf{v}_a \cdot \vf{p}_a\]
\[= \sum_a\left[\total{\vf{p}_a \cdot \vf{r}_a} - \left(\frac{d}{dt}\vf{p}_a\right)\cdot\vf{r}_a\right]\]
\[= \total{\sum_a\vf{p}_a\cdot\vf{r}_a} - \sum_a\dot{\vf{p}}_a\cdot\vf{r}_a\]
Now we average LHS and RHS with respect to time.

Assume the system is executing \underline{bounded motion}: nothing leaves or joins. 
\begin{definition}
For any \(f(t)\), the time average \(\bar{f}\) is given by
\[\bar{f} = \lim_{\tau\to\infty}\frac{1}{\tau}\int_0^\tau dtf(t)\]
\end{definition}
\begin{lemma}
If \(f(t) = \frac{d}{dt}F(t)\) and \(F(t)\) is bounded, then
\[\bar{f} = \lim_{\tau\to\infty}\frac{F(\tau) - F(0)}{\tau} = 0\]
\end{lemma}
So using this, we get, using \(\dot{\vf{p}}_a = \prt{U}{\vf{r}_a}\),
\[2\bar{T} = -\overline{\sum_a\dot{\vf{p}}_a \cdot \vf{r}_a} = \overline{\sum_a\prt{U}{\vf{r}_a}\cdot\vf{r}_a}\]
\[\implies 2\bar{T} = k\bar{U}\]
Say \(\bar{E} = \bar{T} + \bar{U}\),
\[\bar{U} = \frac{2}{2 + k}\bar{E}, \ \ \bar{T} = \frac{k}{2 + k}\bar{E}\]
Examples:
\begin{itemize}
    \item Simple harmonic motion: \(k = 2\): \(\bar{U} = \bar{T} = \frac{1}{2}E\)
    \item Kepler: \(k = -1\): \(\bar{U} = 2\bar{E}\), \(\bar{T} = -\bar{E}\), \(\bar{E} < 0\)
\end{itemize}
\section{Statistical Mechanics: Equipartition of Energy}
We know each translational degree of freedom carries \(\frac{1}{2}kT_{emp}\) of kinetic energy. So
\[\overline{\frac{mv^2}{2}} = \frac{3}{2}kT_{emp} = T_x + T_y + T_z\]
What if the particle has vibrational modes?
\[\frac{1}{2}kT_{emp} = \bar{T}_x = \bar{U}_x = \overline{\frac{m\omega^2x^2}{2}}\]
implying each vibrational mode ALSO sets \(\frac{1}{2}kT_{emp}\) of \(E\).

In a big system, time average = statistical average.

\section{From Lagrangian to Hamiltonian Dynamics}
\begin{itemize}

    \item[Lagrangian mechanics:] \(\lgr(\{q_i, \dot{q}_i\}, t)\)
    \[\implies \frac{d}{dt}\prt{\lgr}{\dot{q}} = \prt{\lgr}{dq}\]
    for every particle, giving \(N\) second order ODE's.
    
    \item[Hamiltonian mechanics:] \(H(\{q(t), p(t)\}, t)\)
    giving \(2N\) first order ODEs.
    
\end{itemize}
Plan:
\begin{enumerate}
    \item Legendre Theorem
    \item Hamiltonian and Hamilton equations
    \item Brief introduction to phase space and phase flow
    \item Poisson brackets (\(\to\) QM)
    \item[] (LL Ch. 7, sections 40, 42 maybe 45, 46, 49)
\end{enumerate}
\end{document}